console.log("--- Day 9: Sensor Boost ---");

const intcode = [
    1102,34463338,34463338,63,1007,63,34463338,63,1005,63,53,1102,1,3,1000,109,988,209,12,9,1000,209,6,209,3,203,0,1008,1000,1,63,1005,63,65,1008,1000,2,63,1005,63,904,1008,1000,0,63,1005,63,58,4,25,104,0,99,4,0,104,0,99,4,17,104,0,99,0,0,1101,0,0,1020,1102,1,800,1023,1101,0,388,1025,1101,0,31,1012,1102,1,1,1021,1101,22,0,1014,1101,0,30,1002,1101,0,716,1027,1102,32,1,1009,1101,0,38,1017,1102,20,1,1015,1101,33,0,1016,1101,0,35,1007,1101,0,25,1005,1102,28,1,1011,1102,1,36,1008,1101,0,39,1001,1102,1,21,1006,1101,397,0,1024,1102,1,807,1022,1101,0,348,1029,1101,0,23,1003,1101,29,0,1004,1102,1,26,1013,1102,34,1,1018,1102,1,37,1010,1101,0,27,1019,1102,24,1,1000,1101,353,0,1028,1101,0,723,1026,109,14,2101,0,-9,63,1008,63,27,63,1005,63,205,1001,64,1,64,1106,0,207,4,187,1002,64,2,64,109,-17,2108,24,6,63,1005,63,223,1105,1,229,4,213,1001,64,1,64,1002,64,2,64,109,7,2101,0,2,63,1008,63,21,63,1005,63,255,4,235,1001,64,1,64,1106,0,255,1002,64,2,64,109,-7,2108,29,7,63,1005,63,273,4,261,1106,0,277,1001,64,1,64,1002,64,2,64,109,10,1208,-5,31,63,1005,63,293,1105,1,299,4,283,1001,64,1,64,1002,64,2,64,109,2,1207,-1,35,63,1005,63,315,1106,0,321,4,305,1001,64,1,64,1002,64,2,64,109,8,1205,3,333,1106,0,339,4,327,1001,64,1,64,1002,64,2,64,109,11,2106,0,0,4,345,1106,0,357,1001,64,1,64,1002,64,2,64,109,-15,21108,40,40,6,1005,1019,379,4,363,1001,64,1,64,1106,0,379,1002,64,2,64,109,16,2105,1,-5,4,385,1001,64,1,64,1105,1,397,1002,64,2,64,109,-25,2102,1,-1,63,1008,63,26,63,1005,63,421,1001,64,1,64,1106,0,423,4,403,1002,64,2,64,109,-8,1202,9,1,63,1008,63,25,63,1005,63,445,4,429,1105,1,449,1001,64,1,64,1002,64,2,64,109,5,1207,0,40,63,1005,63,467,4,455,1106,0,471,1001,64,1,64,1002,64,2,64,109,-6,2107,24,8,63,1005,63,487,1105,1,493,4,477,1001,64,1,64,1002,64,2,64,109,15,21107,41,40,1,1005,1011,509,1106,0,515,4,499,1001,64,1,64,1002,64,2,64,109,12,1205,-1,529,4,521,1105,1,533,1001,64,1,64,1002,64,2,64,109,-20,2102,1,2,63,1008,63,29,63,1005,63,555,4,539,1105,1,559,1001,64,1,64,1002,64,2,64,109,15,1201,-9,0,63,1008,63,38,63,1005,63,579,1105,1,585,4,565,1001,64,1,64,1002,64,2,64,109,-2,21102,42,1,-3,1008,1012,44,63,1005,63,609,1001,64,1,64,1106,0,611,4,591,1002,64,2,64,109,-21,2107,29,8,63,1005,63,629,4,617,1106,0,633,1001,64,1,64,1002,64,2,64,109,15,1202,0,1,63,1008,63,30,63,1005,63,657,1001,64,1,64,1106,0,659,4,639,1002,64,2,64,109,15,21102,43,1,-8,1008,1016,43,63,1005,63,681,4,665,1105,1,685,1001,64,1,64,1002,64,2,64,109,-10,21107,44,45,-4,1005,1010,707,4,691,1001,64,1,64,1106,0,707,1002,64,2,64,109,11,2106,0,2,1001,64,1,64,1106,0,725,4,713,1002,64,2,64,109,-16,21101,45,0,8,1008,1017,43,63,1005,63,749,1001,64,1,64,1105,1,751,4,731,1002,64,2,64,109,-3,1208,2,36,63,1005,63,773,4,757,1001,64,1,64,1106,0,773,1002,64,2,64,109,18,1206,-4,787,4,779,1105,1,791,1001,64,1,64,1002,64,2,64,109,-8,2105,1,7,1001,64,1,64,1106,0,809,4,797,1002,64,2,64,109,-2,21108,46,44,2,1005,1016,825,1105,1,831,4,815,1001,64,1,64,1002,64,2,64,109,7,21101,47,0,-8,1008,1013,47,63,1005,63,857,4,837,1001,64,1,64,1105,1,857,1002,64,2,64,109,-17,1201,-4,0,63,1008,63,24,63,1005,63,883,4,863,1001,64,1,64,1105,1,883,1002,64,2,64,109,10,1206,7,895,1106,0,901,4,889,1001,64,1,64,4,64,99,21102,1,27,1,21102,1,915,0,1105,1,922,21201,1,24405,1,204,1,99,109,3,1207,-2,3,63,1005,63,964,21201,-2,-1,1,21101,942,0,0,1106,0,922,22102,1,1,-1,21201,-2,-3,1,21101,0,957,0,1106,0,922,22201,1,-1,-2,1106,0,968,21201,-2,0,-2,109,-3,2106,0,0
];

const Opcode = {
    add: [1, 4],
    mul: [2, 4],
    set: [3, 2],
    get: [4, 2],
    jmpTrue: [5, 3],
    jmpFalse: [6, 3],
    lt: [7, 4],
    eq: [8, 4],
    base: [9, 2],
    end: 99,
    val0Index: 1,
    val1Index: 2,
    resIndex: 3
};

function getValue(val, program, pos, mode, relativeBase)
{
    return mode[mode.length - val] === 1 ? pos + val
        : mode[mode.length - val] === 2 ? relativeBase + program[pos + val]
        : program[pos + val];
}

function execute(program, index, inputs) {
    let pos = index;
    let output = [];
    let relativeBase = 0;

    while (program[pos] !== Opcode.end)
    {
        const op = program[pos]

        const parsed = String(op).match(/(\d*?)(\d{1,2})$/)                     // gets the opcode as string parsed[2] and the parameter mode parsed[1]
        const opCode = parseInt(parsed[2])                                      // converts the op code part back into an integer
        const mode = parsed[1].split('').map((item, index) => parseInt(item))   // converts the mode part from string to array of integers

        const val0 = getValue(Opcode.val0Index, program, pos, mode, relativeBase);
        const val1 = getValue(Opcode.val1Index, program, pos, mode, relativeBase);
        const resultIndex = getValue(Opcode.resIndex, program, pos, mode, relativeBase);

        switch(opCode) {
            case Opcode.add[0]:
                program[resultIndex] = program[val0] + program[val1];
                pos += Opcode.add[1];
                break;
            case Opcode.mul[0]:
                program[resultIndex] = program[val0] * program[val1];
                pos += Opcode.mul[1];
                break;
            case Opcode.set[0]:
                if (inputs.length === 0)
                    return [output, pos, true];
                program[val0] = inputs.shift();
                pos += Opcode.set[1];
                break;
            case Opcode.get[0]:
                output.push(program[val0]);
                pos += Opcode.get[1];
                break;
            case Opcode.jmpTrue[0]:
                if (program[val0] !== 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpTrue[1];
                break;
            case Opcode.jmpFalse[0]:
                if (program[val0] === 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpFalse[1];
                break;
            case Opcode.lt[0]:
                program[resultIndex] = (program[val0] < program[val1]) ? 1 : 0;
                pos += Opcode.lt[1];
                break;
            case Opcode.eq[0]:
                program[resultIndex] = (program[val0] === program[val1]) ? 1 : 0;
                pos += Opcode.eq[1];
                break;
            case Opcode.base[0]:
                relativeBase += program[val0];
                pos += Opcode.base[1];
                break;
            default:
                console.log("error: encountered unhandled opcode " + opCode);
                return -1;
        }
    }

    return [output, pos, false];
}

function arraysMatch (first, second) {
    if (first.length === second.length)
        return first.every((elem, index) => elem === second[index]);
    return false;
}

function getThrusterSignal(program, signal) {
    let thrust = 0;
    for (let i = 0; i < signal.length; ++i)
        thrust = execute([...program], 0, [signal[i], thrust])[0];
    return thrust;
}

function getThrusterFeedbackSignal(program, phase) {
    let pos = [0, 0, 0, 0, 0];
    let prog = [[...program], [...program], [...program], [...program], [...program]];
    let running = [true, true, true, true, true];
    let input = [...phase];
    let firstRun = [true, true, true, true, true];

    console.assert(phase.length === pos.length);

    // starting from 0

    // 5 different copies of the program, keeping track of the position returned
    // on first run, the input for each amp is the phase setting, then every
    // input is the signal.

    let signal = 0;

    while (running.some(value => value === true)) {
        for (let i = 0; i < phase.length; ++i) {
            if (running[i])
            {
                let result = firstRun[i] ? execute(prog[i], pos[i], [input[i], signal])
                                         : execute(prog[i], pos[i], [signal]);
                signal = result[0].slice(-1).pop();
                pos[i] = result[1];
                running[i] = result[2];
                firstRun[i] = false;
            }
        }
    }

    return signal;
}

// from stack exchange https://stackoverflow.com/a/40655691/2748376
function combo(c) {
    var r = [],
        len = c.length;
    tmp = [];
    function nodup() {
        var got = {};
        for (var l = 0; l < tmp.length; l++) {
            if (got[tmp[l]]) return false;
            got[tmp[l]] = true;
        }
        return true;
    }
    function iter(col, done) {
        var l, rr;
        if (col === len) {
            if (nodup()) {
                rr = [];
                for (l = 0; l < tmp.length; l++)
                    rr.push(c[tmp[l]]);
                r.push(rr);
            }
        } else {
            for (l = 0; l < len; l++) {
                tmp[col] = l;
                iter(col + 1);
            }
        }
    }
    iter(0);
    return r;
}

let phaseCombinations = combo([0, 1, 2, 3, 4]);
let phaseFeedbackCombinations = combo([5, 6, 7, 8, 9]);

function getMaxThrusterSignal(program) {
    let maxSoFar = 0;
    for (let i = 0; i < phaseCombinations.length; ++i)
        maxSoFar = Math.max(maxSoFar, getThrusterSignal(program, phaseCombinations[i]));
    return maxSoFar;
}

function getMaxFeedbackThrusterSignal(program) {
    let maxSoFar = 0;
    for (let i = 0; i < phaseFeedbackCombinations.length; ++i)
        maxSoFar = Math.max(maxSoFar, getThrusterFeedbackSignal(program, phaseFeedbackCombinations[i]));
    return maxSoFar;
}

function testsPass() {
    let testInput = [
        [1,0,0,0,99],
        [2,3,0,3,99],
        [2,4,4,5,99,0],
        [1,1,1,4,99,5,6,0,99]
    ];

    const testResult = [
        [2,0,0,0,99],
        [2,3,0,6,99],
        [2,4,4,5,99,9801],
        [30,1,1,4,2,5,6,0,99]
    ];

    for (let i = 0; i < testInput.length; ++i)
    {
        execute(testInput[i], 0)[0];
        if (! arraysMatch (testInput[i], testResult[i]))
        {
            console.log("error: " + testInput[i] + " should be " + testResult[i])
            return false;
        }
    }

    for (let i = 0; i < 100; ++i)
    {
        const output = execute([3,0,4,0,99], 0, [i])[0];
        if (output != i)
        {
            console.log("error: " + output + " should be " + i);
            return false;
        }
    }

    execute([1002,4,3,4,33], 0)[0];
    execute([1101,100,-1,4,0], 0)[0];

    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, [8])[0] == 1);
    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, [0])[0] == 0);

    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, [7])[0] == 1);
    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, [8])[0] == 0);

    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, [8])[0] == 1);
    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, [0])[0] == 0);

    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, [0])[0] == 0);
    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, [1])[0] == 1);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, [8])[0] == 1000);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, [9])[0] == 1001);

    let phaseTestInput = [
        [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0],
        [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0],
        [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]
    ];

    const phaseTestSignal = [
        [4,3,2,1,0],
        [0,1,2,3,4],
        [1,0,4,3,2]
    ];

    const phaseTestResult = [
        43210,
        54321,
        65210
    ];

    for (let i = 0; i < phaseTestInput.length; ++i)
    {
        const output = getThrusterSignal(phaseTestInput[i], phaseTestSignal[i]);
        if (output != phaseTestResult[i])
        {
            console.log("error: " + output + " should be " + phaseTestResult[i])
            return false;
        }

        console.assert(getMaxThrusterSignal(phaseTestInput[i]) == phaseTestResult[i]);
    }

    let phaseFeedbackTestInput = [
        [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5],
        [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]
    ];

    const phaseFeedbackTestSignal = [
        [9,8,7,6,5],
        [9,7,8,5,6]
    ];

    const phaseFeedbackTestResult = [
        139629729,
        18216
    ];

    for (let i = 0; i < phaseFeedbackTestInput.length; ++i) {
        let output = getMaxFeedbackThrusterSignal(phaseFeedbackTestInput[i]);
        if (output != phaseFeedbackTestResult[i]) {
            console.log("error: " + output + " should be " + phaseFeedbackTestResult[i])
            return false;
        }

        console.assert(getMaxFeedbackThrusterSignal(phaseFeedbackTestInput[i]) == phaseFeedbackTestResult[i]);
    }

    // let op9Test = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
    // console.assert(execute(op9Test, 0)[0][0] == op9Test);
    console.assert(execute([1102,34915192,34915192,7,4,7,99,0], 0)[0][0].toString().length == 16);
    console.assert(execute([104,1125899906842624,99], 0)[0][0] == 1125899906842624);

    return true;
}

function part1() {
    let inputCopy = [...intcode];

    const output = execute(inputCopy, 0, [1])[0];

    console.log(`Part 1: ${output}`);
}

function part2() {
    let inputCopy = [...intcode];

    const output = execute(inputCopy, 0, [2])[0];

    console.log(`Part 2: ${output}`);
}



console.assert(testsPass(), "tests failed");

part1();
part2();
