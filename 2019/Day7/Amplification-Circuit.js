console.log("--- Day 7: Amplification Circuit ---");

const intcode = [
    3,8,1001,8,10,8,105,1,0,0,21,42,63,76,101,114,195,276,357,438,99999,3,9,101,2,9,9,102,5,9,9,1001,9,3,9,1002,9,5,9,4,9,99,3,9,101,4,9,9,102,5,9,9,1001,9,5,9,102,2,9,9,4,9,99,3,9,1001,9,3,9,1002,9,5,9,4,9,99,3,9,1002,9,2,9,101,5,9,9,102,3,9,9,101,2,9,9,1002,9,3,9,4,9,99,3,9,101,3,9,9,102,2,9,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,99
];

const Opcode = {
    add: [1, 4],
    mul: [2, 4],
    set: [3, 2],
    get: [4, 2],
    jmpTrue: [5, 3],
    jmpFalse: [6, 3],
    lt: [7, 4],
    eq: [8, 4],
    end: 99,
    val0Index: 1,
    val1Index: 2,
    resIndex: 3
};

function execute(program, index, inputs) {
    let pos = index;
    let output = 0;

    while (program[pos] != Opcode.end)
    {
        const op = program[pos]

        const parsed = String(op).match(/(\d*?)(\d{1,2})$/)                     // gets the opcode as string parsed[2] and the parameter mode parsed[1]
        const opCode = parseInt(parsed[2])                                      // converts the op code part back into an integer
        const mode = parsed[1].split('').map((item, index) => parseInt(item))   // converts the mode part from string to array of integers

        const val0 = mode[mode.length - Opcode.val0Index] == 1 ? pos + Opcode.val0Index : program[pos + Opcode.val0Index];
        const val1 = mode[mode.length - Opcode.val1Index] == 1 ? pos + Opcode.val1Index : program[pos + Opcode.val1Index];
        const resultIndex = mode[mode.length - Opcode.resIndex] == 1 ? pos + Opcode.resIndex : program[pos + Opcode.resIndex];

        switch(opCode) {
            case Opcode.add[0]:
                program[resultIndex] = program[val0] + program[val1];
                pos += Opcode.add[1];
                break;
            case Opcode.mul[0]:
                program[resultIndex] = program[val0] * program[val1];
                pos += Opcode.mul[1];
                break;
            case Opcode.set[0]:
                if (inputs.length === 0)
                    return [output, pos, true];
                program[val0] = inputs.shift();
                pos += Opcode.set[1];
                break;
            case Opcode.get[0]:
                output = program[val0];
                pos += Opcode.get[1];
                break;
            case Opcode.jmpTrue[0]:
                if (program[val0] != 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpTrue[1];
                break;
            case Opcode.jmpFalse[0]:
                if (program[val0] == 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpFalse[1];
                break;
            case Opcode.lt[0]:
                program[resultIndex] = (program[val0] < program[val1]) ? 1 : 0;
                pos += Opcode.lt[1];
                break;
            case Opcode.eq[0]:
                program[resultIndex] = (program[val0] == program[val1]) ? 1 : 0;
                pos += Opcode.eq[1];
                break;
            default:
                console.log("error: encountered unhandled opcode " + op);
                return -1;
        }
    }

    return [output, pos, false];
}

function arraysMatch (first, second) {
    if (first.length === second.length)
        return first.every((elem, index) => elem === second[index]);
    return false;
}

function getThrusterSignal(program, signal) {
    let thrust = 0;
    for (let i = 0; i < signal.length; ++i)
        thrust = execute([...program], 0, [signal[i], thrust])[0];
    return thrust;
}

function getThrusterFeedbackSignal(program, phase) {
    let pos = [0, 0, 0, 0, 0];
    let prog = [[...program], [...program], [...program], [...program], [...program]];
    let running = [true, true, true, true, true];
    let input = [...phase];
    let firstRun = [true, true, true, true, true];

    console.assert(phase.length === pos.length);

    // starting from 0

    // 5 different copies of the program, keeping track of the position returned
    // on first run, the input for each amp is the phase setting, then every
    // input is the signal.

    let signal = 0;

    while (running.some(value => value === true)) {
        for (let i = 0; i < phase.length; ++i) {
            if (running[i])
            {
                let result = firstRun[i] ? execute(prog[i], pos[i], [input[i], signal])
                                         : execute(prog[i], pos[i], [signal]);
                signal = result[0];
                pos[i] = result[1];
                running[i] = result[2];
                firstRun[i] = false;
            }
        }
    }

    return signal;
}

// from stack exchange https://stackoverflow.com/a/40655691/2748376
function combo(c) {
    var r = [],
        len = c.length;
    tmp = [];
    function nodup() {
        var got = {};
        for (var l = 0; l < tmp.length; l++) {
            if (got[tmp[l]]) return false;
            got[tmp[l]] = true;
        }
        return true;
    }
    function iter(col, done) {
        var l, rr;
        if (col === len) {
            if (nodup()) {
                rr = [];
                for (l = 0; l < tmp.length; l++)
                    rr.push(c[tmp[l]]);
                r.push(rr);
            }
        } else {
            for (l = 0; l < len; l++) {
                tmp[col] = l;
                iter(col + 1);
            }
        }
    }
    iter(0);
    return r;
}

let phaseCombinations = combo([0, 1, 2, 3, 4]);
let phaseFeedbackCombinations = combo([5, 6, 7, 8, 9]);

function getMaxThrusterSignal(program) {
    let maxSoFar = 0;
    for (let i = 0; i < phaseCombinations.length; ++i)
        maxSoFar = Math.max(maxSoFar, getThrusterSignal(program, phaseCombinations[i]));
    return maxSoFar;
}

function getMaxFeedbackThrusterSignal(program) {
    let maxSoFar = 0;
    for (let i = 0; i < phaseFeedbackCombinations.length; ++i)
        maxSoFar = Math.max(maxSoFar, getThrusterFeedbackSignal(program, phaseFeedbackCombinations[i]));
    return maxSoFar;
}

function testsPass() {
    let testInput = [
        [1,0,0,0,99],
        [2,3,0,3,99],
        [2,4,4,5,99,0],
        [1,1,1,4,99,5,6,0,99]
    ];

    const testResult = [
        [2,0,0,0,99],
        [2,3,0,6,99],
        [2,4,4,5,99,9801],
        [30,1,1,4,2,5,6,0,99]
    ];

    for (let i = 0; i < testInput.length; ++i)
    {
        execute(testInput[i], 0)[0];
        if (! arraysMatch (testInput[i], testResult[i]))
        {
            console.log("error: " + testInput[i] + " should be " + testResult[i])
            return false;
        }
    }

    for (let i = 0; i < 100; ++i)
    {
        const output = execute([3,0,4,0,99], 0, [i])[0];
        if (output != i)
        {
            console.log("error: " + output + " should be " + i);
            return false;
        }
    }

    execute([1002,4,3,4,33], 0)[0];
    execute([1101,100,-1,4,0], 0)[0];

    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, [8])[0] == 1);
    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, [0])[0] == 0);

    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, [7])[0] == 1);
    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, [8])[0] == 0);

    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, [8])[0] == 1);
    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, [0])[0] == 0);

    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, [0])[0] == 0);
    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, [1])[0] == 1);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, [8])[0] == 1000);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, [9])[0] == 1001);

    let phaseTestInput = [
        [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0],
        [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0],
        [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]
    ];

    const phaseTestSignal = [
        [4,3,2,1,0],
        [0,1,2,3,4],
        [1,0,4,3,2]
    ];

    const phaseTestResult = [
        43210,
        54321,
        65210
    ];

    for (let i = 0; i < phaseTestInput.length; ++i)
    {
        const output = getThrusterSignal(phaseTestInput[i], phaseTestSignal[i]);
        if (output != phaseTestResult[i])
        {
            console.log("error: " + output + " should be " + phaseTestResult[i])
            return false;
        }

        console.assert(getMaxThrusterSignal(phaseTestInput[i]) == phaseTestResult[i]);
    }

    let phaseFeedbackTestInput = [
        [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5],
        [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10]
    ];

    const phaseFeedbackTestSignal = [
        [9,8,7,6,5],
        [9,7,8,5,6]
    ];

    const phaseFeedbackTestResult = [
        139629729,
        18216
    ];

    for (let i = 0; i < phaseFeedbackTestInput.length; ++i) {
        let output = getMaxFeedbackThrusterSignal(phaseFeedbackTestInput[i]);
        if (output != phaseFeedbackTestResult[i]) {
            console.log("error: " + output + " should be " + phaseFeedbackTestResult[i])
            return false;
        }

        console.assert(getMaxFeedbackThrusterSignal(phaseFeedbackTestInput[i]) == phaseFeedbackTestResult[i]);
    }

    return true;
}

function part1() {
    let part1Input = [...intcode];

    const output = getMaxThrusterSignal(part1Input);

    console.log(`Part 1: ${output}`);
}

function part2() {
    let part2Input = [...intcode];

    const output = getMaxFeedbackThrusterSignal(part2Input);

    console.log(`Part 2: ${output}`);
}



console.assert(testsPass(), "tests failed");

part1();
part2();
