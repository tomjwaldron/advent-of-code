(write-line "--- Day 4: Secure Container ---")

(defconstant lower-limit 240920)
(defconstant upper-limit 789857)

(defun number->digits (number)
    (loop for c across (write-to-string number) collect (digit-char-p c))
)

(defun digits->number (digits)
    (reduce (lambda (a b) (+ (* 10 a) b)) digits)
)

(defun has-matching-pair (number)
    (setq digits (number->digits number))
    (loop for i from 0 to (- (list-length digits) 2)
        do (if (= (nth i digits) (nth (+ i 1) digits)) (return t))
    )
)

(defun get-number-of (value list)
    (loop for item in list
        counting (= item value) into count
        finally (return count)
    )
)

(defun has-matching-pair-inc (number)
    (setq digits (number->digits number))
    (loop for (a b) on digits by #'cdr
        if (and (not (null b)) (= a b) (= (get-number-of a digits) 2))
            return t
    )
)

(defun never-decreases (number)
    (setq digits (number->digits number))
    (every #'<= digits (rest digits))
)

(defun number-meets-criteria (number)
    (and (never-decreases number)
         (has-matching-pair number)
    )
)

(defun get-lowest-valid (number)
    (digits->number (loop for digit in (number->digits number)
        maximize digit into current-max
        collect current-max
    ))
)

(defun get-highest-valid (number)
    (digits->number (loop for digit in (number->digits number)
        maximize digit into current-max
        collect current-max
    ))
)

(defun num-passwords (criteria)
    (loop for i from lower-limit upto upper-limit
        counting (funcall criteria i) into count
        finally (return count)
    )
)

(assert (not (has-matching-pair 123456)))
(assert (has-matching-pair 112345))
(assert (has-matching-pair 123455))

(assert (never-decreases 123456))
(assert (not (never-decreases 654321)))
(assert (not (never-decreases 111110)))

(assert (never-decreases 111111))
(assert (not (never-decreases 223450)))
(assert (not (has-matching-pair 123789)))

(assert (= (digits->number (number->digits 123456)) 123456))

(assert (has-matching-pair 123444))
(assert (not (has-matching-pair-inc 123444)))

(write (format t "Part 1: ~D" (num-passwords
    #'(lambda (n) (and (never-decreases n) (has-matching-pair n))))))
(terpri)
(write (format t "Part 2: ~D" (num-passwords
    #'(lambda (n) (and (never-decreases n) (has-matching-pair-inc n))))))
(terpri)
