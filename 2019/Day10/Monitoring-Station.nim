echo "--- Day 10: Monitoring Station ---"

import strutils, sequtils, math, algorithm, sugar

type
    Pos = object
        x: int
        y: int
    Result = tuple
        position: Pos
        numVisible: int
    PosAngleDist = object
        position: Pos
        angle: float
        distance: float

template parse(s: string): untyped =
    const input = s.unindent().replace('.', 'n').replace('#', 'y').splitLines()
    var result: array[input.len(), array[input[0].len(), bool]]
    for x, line in input:
        for y, c in line:
            result[x][y] = parseBool($c)
    result

const
    testInput1 = parse(""".#..#
                          .....
                          #####
                          ....#
                          ...##""")
    testResult1: Result = (position: Pos(x: 3, y: 4), numVisible: 8)
    testInput2 = parse("""......#.#.
                          #..#.#....
                          ..#######.
                          .#.#.###..
                          .#..#.....
                          ..#....#.#
                          #..#....#.
                          .##.#..###
                          ##...#..#.
                          .#....####""")
    testResult2: Result = (position: Pos(x: 5, y: 8), numVisible: 33)
    testInput3 = parse("""#.#...#.#.
                          .###....#.
                          .#....#...
                          ##.#.#.#.#
                          ....#.#.#.
                          .##..###.#
                          ..#...##..
                          ..##....##
                          ......#...
                          .####.###.""")
    testResult3: Result = (position: Pos(x: 1, y: 2), numVisible: 35)
    testInput4 = parse(""".#..#..###
                          ####.###.#
                          ....###.#.
                          ..###.##.#
                          ##.##.#.#.
                          ....###..#
                          ..#.#..#.#
                          #..#.#.###
                          .##...##.#
                          .....#.#..""")
    testResult4: Result = (position: Pos(x: 6, y: 3), numVisible: 41)
    testInput5 = parse(""".#..##.###...#######
                          ##.############..##.
                          .#.######.########.#
                          .###.#######.####.#.
                          #####.##.#.##.###.##
                          ..#####..#.#########
                          ####################
                          #.####....###.#.#.##
                          ##.#################
                          #####.##.###..####..
                          ..######..##.#######
                          ####.##.####...##..#
                          .#####..#.######.###
                          ##...#.##########...
                          #.##########.#######
                          .####.#.###.###.#.##
                          ....##.##.###..#####
                          .#.#.###########.###
                          #.#.#.#####.####.###
                          ###.##.####.##.#..##""")
    testResult5: Result = (position: Pos(x: 11, y: 13), numVisible: 210)
    puzzleInput = parse(""".###..#......###..#...#
                           #.#..#.##..###..#...#.#
                           #.#.#.##.#..##.#.###.##
                           .#..#...####.#.##..##..
                           #.###.#.####.##.#######
                           ..#######..##..##.#.###
                           .##.#...##.##.####..###
                           ....####.####.#########
                           #.########.#...##.####.
                           .#.#..#.#.#.#.##.###.##
                           #..#.#..##...#..#.####.
                           .###.#.#...###....###..
                           ###..#.###..###.#.###.#
                           ...###.##.#.##.#...#..#
                           #......#.#.##..#...#.#.
                           ###.##.#..##...#..#.#.#
                           ###..###..##.##..##.###
                           ###.###.####....######.
                           .###.#####.#.#.#.#####.
                           ##.#.###.###.##.##..##.
                           ##.#..#..#..#.####.#.#.
                           .#.#.#.##.##########..#
                           #####.##......#.#.####.""")

iterator scan(rows, cols: int): Pos =
    for row in 0..<rows:
        for col in 0..<cols:
            yield Pos(x: row, y: col)

template findAsteroids(input: untyped): untyped =
    var asteroids: seq[Pos]
    for pos in scan(input[0].len(), input.len()):
        if input[pos.y][pos.x]:
            asteroids.add(pos)
    asteroids

# This is important for part 2, arctan2(dy, dx) would give the angle, but from
# the wrong starting point; arctan2(dx, dy) will give the angle from the top.
proc getAngle(a, b: Pos): float = arctan2(float(b.x - a.x), float(b.y - a.y))

template bestPosition(input: untyped): Result =
    var
        currentMax = Pos(x: 0, y: 0)
        currentCount = 0
    let asteroids = findAsteroids(input)
    for a in asteroids:
        var
            count = 0
            angles: seq[float]
        for b in asteroids.filterIt(it != a):
            angles.add(getAngle(a, b))
        count = angles.deduplicate().len()
        currentCount = max(count, currentCount)
        if currentCount == count:
            currentMax = a
    (currentMax, currentCount)

# part 1 tests
doAssert bestPosition(testInput1) == testResult1
doAssert bestPosition(testInput2) == testResult2
doAssert bestPosition(testInput3) == testResult3
doAssert bestPosition(testInput4) == testResult4
doAssert bestPosition(testInput5) == testResult5

let part1Result = bestPosition(puzzleInput)
echo "Part 1: ", part1Result

proc getDistance(a, b: Pos): float = hypot(float(a.x - b.x), float(a.y - b.y))

proc getAnglesDistance(positions: seq[Pos], origin: Pos): seq[PosAngleDist] =
    for pos in positions:
        result.add(PosAngleDist(position: pos,
                                angle: getAngle(origin, pos),
                                distance: getDistance(origin, pos)))

proc groupByAngle(list: seq[PosAngleDist]): seq[seq[PosAngleDist]] =
    var
        group = 0
        angle = list[0].angle
    for i in list:
        if i.angle != angle:
            inc(group)
            result.add(@[])
            angle = i.angle
        if group >= result.len():
            result.add(@[])
        result[group].add(i)

template twoHundredthAsteroid(input: untyped, origin: Pos): int =
    let asteroids = findAsteroids(input).filter do (x: Pos) -> bool: x != origin
    var posAngleDist = getAnglesDistance(asteroids, origin)

    posAngleDist.sort((x, y) => cmp(y.distance, x.distance))
    posAngleDist.sort((x, y) => cmp(y.angle, x.angle))

    var
        groupedByAngle = groupByAngle(posAngleDist)
        resultPos: Pos

    if groupedByAngle.len() < 200:
        # NOTE: untested and assumes that only one further iteration is needed
        let nextIteration = groupedByAngle.filter do (x: seq[PosAngleDist]) -> bool: x.len() > 1
        resultPos = groupedByAngle[200 - groupedByAngle.len()][1].position
    else:
        resultPos = groupedByAngle[200 - 1][0].position

    resultPos.x * 100 + resultPos.y

# part 2 test
doAssert twoHundredthAsteroid(testInput5, testResult5.position) == 802

let part2Result = twoHundredthAsteroid(puzzleInput, part1Result.position)
echo "Part 2: ", part2Result
