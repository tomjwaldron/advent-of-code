const intcode = [
    3,225,1,225,6,6,1100,1,238,225,104,0,1101,37,34,224,101,-71,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1002,113,50,224,1001,224,-2550,224,4,224,1002,223,8,223,101,2,224,224,1,223,224,223,1101,13,50,225,102,7,187,224,1001,224,-224,224,4,224,1002,223,8,223,1001,224,5,224,1,224,223,223,1101,79,72,225,1101,42,42,225,1102,46,76,224,101,-3496,224,224,4,224,102,8,223,223,101,5,224,224,1,223,224,223,1102,51,90,225,1101,11,91,225,1001,118,49,224,1001,224,-140,224,4,224,102,8,223,223,101,5,224,224,1,224,223,223,2,191,87,224,1001,224,-1218,224,4,224,1002,223,8,223,101,4,224,224,1,224,223,223,1,217,83,224,1001,224,-124,224,4,224,1002,223,8,223,101,5,224,224,1,223,224,223,1101,32,77,225,1101,29,80,225,101,93,58,224,1001,224,-143,224,4,224,102,8,223,223,1001,224,4,224,1,223,224,223,1101,45,69,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,7,226,226,224,102,2,223,223,1005,224,329,101,1,223,223,108,677,226,224,102,2,223,223,1005,224,344,1001,223,1,223,1108,226,677,224,102,2,223,223,1005,224,359,1001,223,1,223,8,677,226,224,102,2,223,223,1006,224,374,1001,223,1,223,107,226,226,224,102,2,223,223,1006,224,389,101,1,223,223,1108,677,226,224,1002,223,2,223,1005,224,404,1001,223,1,223,108,677,677,224,102,2,223,223,1005,224,419,101,1,223,223,7,226,677,224,1002,223,2,223,1006,224,434,1001,223,1,223,107,226,677,224,102,2,223,223,1005,224,449,101,1,223,223,1108,677,677,224,1002,223,2,223,1006,224,464,101,1,223,223,7,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1007,677,677,224,1002,223,2,223,1005,224,494,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,509,1001,223,1,223,107,677,677,224,102,2,223,223,1006,224,524,1001,223,1,223,8,226,226,224,1002,223,2,223,1005,224,539,1001,223,1,223,1007,677,226,224,102,2,223,223,1006,224,554,1001,223,1,223,1007,226,226,224,1002,223,2,223,1005,224,569,1001,223,1,223,8,226,677,224,1002,223,2,223,1006,224,584,101,1,223,223,108,226,226,224,1002,223,2,223,1006,224,599,101,1,223,223,1107,677,226,224,1002,223,2,223,1005,224,614,1001,223,1,223,1107,226,677,224,102,2,223,223,1006,224,629,1001,223,1,223,1008,226,677,224,102,2,223,223,1005,224,644,101,1,223,223,1107,226,226,224,102,2,223,223,1006,224,659,1001,223,1,223,1008,677,677,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226
];

const Opcode = {
    add: [1, 4],
    mul: [2, 4],
    set: [3, 2],
    get: [4, 2],
    jmpTrue: [5, 3],
    jmpFalse: [6, 3],
    lt: [7, 4],
    eq: [8, 4],
    end: 99,
    val0Index: 1,
    val1Index: 2,
    resIndex: 3
};

function execute(program, index, input) {
    let pos = index;
    let output = 0;

    while (program[pos] != Opcode.end)
    {
        const op = program[pos]

        const parsed = String(op).match(/(\d*?)(\d{1,2})$/)                     // gets the opcode as string parsed[2] and the parameter mode parsed[1]
        const opCode = parseInt(parsed[2])                                      // converts the op code part back into an integer
        const mode = parsed[1].split('').map((item, index) => parseInt(item))   // converts the mode part from string to array of integers

        const val0 = mode[mode.length - Opcode.val0Index] == 1 ? pos + Opcode.val0Index : program[pos + Opcode.val0Index];
        const val1 = mode[mode.length - Opcode.val1Index] == 1 ? pos + Opcode.val1Index : program[pos + Opcode.val1Index];
        const resultIndex = mode[mode.length - Opcode.resIndex] == 1 ? pos + Opcode.resIndex : program[pos + Opcode.resIndex];

        switch(opCode) {
            case Opcode.add[0]:
                program[resultIndex] = program[val0] + program[val1];
                pos += Opcode.add[1];
                break;
            case Opcode.mul[0]:
                program[resultIndex] = program[val0] * program[val1];
                pos += Opcode.mul[1];
                break;
            case Opcode.set[0]:
                program[val0] = input;
                pos += Opcode.set[1];
                break;
            case Opcode.get[0]:
                output = program[val0];
                pos += Opcode.get[1];
                break;
            case Opcode.jmpTrue[0]:
                if (program[val0] != 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpTrue[1];
                break;
            case Opcode.jmpFalse[0]:
                if (program[val0] == 0)
                    pos = program[val1];
                else
                    pos += Opcode.jmpFalse[1];
                break;
            case Opcode.lt[0]:
                program[resultIndex] = (program[val0] < program[val1]) ? 1 : 0;
                pos += Opcode.lt[1];
                break;
            case Opcode.eq[0]:
                program[resultIndex] = (program[val0] == program[val1]) ? 1 : 0;
                pos += Opcode.eq[1];
                break;
            default:
                console.log("error: encountered unhandled opcode " + op);
                return -1;
        }
    }

    return output;
}

function arraysMatch (first, second) {
    if (first.length === second.length)
        return first.every((elem, index) => elem === second[index]);
    return false;
}

function testsPass() {
    let testInput = [
        [1,0,0,0,99],
        [2,3,0,3,99],
        [2,4,4,5,99,0],
        [1,1,1,4,99,5,6,0,99]
    ];

    const testResult = [
        [2,0,0,0,99],
        [2,3,0,6,99],
        [2,4,4,5,99,9801],
        [30,1,1,4,2,5,6,0,99]
    ];

    for (let i = 0; i < testInput.length; ++i)
    {
        execute(testInput[i], 0);
        if (! arraysMatch (testInput[i], testResult[i]))
        {
            console.log("error: " + testInput[i] + " should be " + testResult[i])
            return false;
        }
    }

    for (let i = 0; i < 100; ++i)
    {
        const output = execute([3,0,4,0,99], 0, i);
        if (output != i)
        {
            console.log("error: " + output + " should be " + i);
            return false;
        }
    }

    execute([1002,4,3,4,33], 0);
    execute([1101,100,-1,4,0], 0);

    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, 8) == 1);
    console.assert(execute([3,9,8,9,10,9,4,9,99,-1,8], 0, 0) == 0);

    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, 7) == 1);
    console.assert(execute([3,9,7,9,10,9,4,9,99,-1,8], 0, 8) == 0);

    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, 8) == 1);
    console.assert(execute([3,3,1108,-1,8,3,4,3,99], 0, 0) == 0);

    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, 0) == 0);
    console.assert(execute([3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0, 1) == 1);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, 8) == 1000);

    console.assert(execute([3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                            0, 9) == 1001);

    return true;
}

function part1() {
    let part1Input = [...intcode];
    let userInput = 1

    const output = execute(part1Input, 0, userInput);

    console.log(`Part 1: ${output}`);
}

function part2() {
    let part1Input = [...intcode];
    let userInput = 5

    const output = execute(part1Input, 0, userInput);

    console.log(`Part 2: ${output}`);
}

console.log("--- Day 5: Sunny with a Chance of Asteroids ---");

console.assert(testsPass(), "tests failed");

part1();
part2()
