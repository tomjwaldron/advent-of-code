const input = [
    1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,9,23,1,23,5,27,2,6,27,31,1,31,
    5,35,1,35,5,39,2,39,6,43,2,43,10,47,1,47,6,51,1,51,6,55,2,55,6,59,1,10,59,
    63,1,5,63,67,2,10,67,71,1,6,71,75,1,5,75,79,1,10,79,83,2,83,10,87,1,87,9,91,
    1,91,10,95,2,6,95,99,1,5,99,103,1,103,13,107,1,107,10,111,2,9,111,115,1,115,
    6,119,2,13,119,123,1,123,6,127,1,5,127,131,2,6,131,135,2,6,135,139,1,139,5,
    143,1,143,10,147,1,147,2,151,1,151,13,0,99,2,0,14,0
];

const Opcode = {
    add: 1,
    mul: 2,
    end: 99,
    size: 4,
    val0Index: 1,
    val1Index: 2,
    resIndex: 3
};

function execute(program, index) {
    let pos = index;

    while (program[pos] != Opcode.end)
    {
        const val0 = program[pos + Opcode.val0Index];
        const val1 = program[pos + Opcode.val1Index];
        const resultIndex = program[pos + Opcode.resIndex];

        if (program[pos] == Opcode.add)
            program[resultIndex] = program[val0] + program[val1];
        else if (program[pos] == Opcode.mul)
            program[resultIndex] = program[val0] * program[val1];
        else
            console.log("error: encountered unknown opcode " + program[pos]);

        pos += Opcode.size;
    }
}

function arraysMatch (first, second) {
    if (first.length === second.length)
        return first.every((elem, index) => elem === second[index]);
    return false;
}

function testsPass() {
    let testInput = [
        [1,0,0,0,99],
        [2,3,0,3,99],
        [2,4,4,5,99,0],
        [1,1,1,4,99,5,6,0,99]
    ];
    const testResult = [
        [2,0,0,0,99],
        [2,3,0,6,99],
        [2,4,4,5,99,9801],
        [30,1,1,4,2,5,6,0,99]
    ];

    for (let i = 0; i < testInput.length; ++i)
    {
        execute(testInput[i], 0);
        if (! arraysMatch (testInput[i], testResult[i]))
        {
            console.log("error: " + testInput[i] + " should be " + testResult[i])
            return false;
        }
    }

    return true;
}

function part1() {
    let part1Input = [...input];

    // "before running the program, replace position 1 with the value 12
    // and replace position 2 with the value 2"
    part1Input[1] = 12;
    part1Input[2] = 2;

    execute(part1Input, 0);

    return "Part 1: " + part1Input[0];
}

function part2() {
    const output = 19690720;

    for (let noun = 0; noun <= 99; ++noun)
    {
        for (let verb = 0; verb <= 99; ++ verb)
        {
            let program = [...input];
            program[1] = noun;
            program[2] = verb;

            execute(program, 0);

            let result = program[0];

            let answer = 100 * noun + verb;

            if ((noun === 12) && (verb === 2))
                console.assert(answer === 1202, "noun verb execusion failed");

            if (result === output)
                return "Part 2: " + answer;
        }
    }

    return "Part 2: Failed";
}

console.log("Day 2: 1202 Program Alarm");

console.assert(testsPass(), "tests failed");

console.log(part1());
console.log(part2());
