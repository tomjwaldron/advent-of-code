// clang++ -std=c++17 -Wall -O3 Universal-Orbit-Map.cpp -o Universal-Orbit-Map && ./Universal-Orbit-Map

#include <iostream>
#include <array>
#include <string_view>
#include <cassert>
#include <algorithm>
#include <vector>
#include <set>
#include <iterator>

using namespace std::literals;

template<typename ArrayType>
struct Map
{
    std::vector<std::size_t> inside, outside;
    std::size_t comHash = 0, comIndex = 0;
    std::size_t youHash = 0, youIndex = 0;
    std::size_t sanHash = 0, sanIndex = 0;

    Map(ArrayType& map)
    {
        std::size_t counter = 0;

        // parse the input strings into inside and outside orbits
        std::for_each(map.begin(), map.end(), [&](auto &orbit) {
            const auto split = orbit.find(')');
            const auto inner = orbit.substr(0, split);
            const auto outer = orbit.substr(split + 1);
            const auto innerHash = std::hash<std::string_view>()(inner);
            const auto outerHash = std::hash<std::string_view>()(outer);

            inside.push_back(innerHash);
            outside.push_back(outerHash);

            if (inner.compare("COM"sv) == 0)
            {
                comHash = innerHash;
                comIndex = counter;
            }

            if (outer.compare("YOU"sv) == 0)
            {
                youHash = outerHash;
                youIndex = counter;
            }

            if (outer.compare("SAN"sv) == 0)
            {
                sanHash = outerHash;
                sanIndex = counter;
            }

            counter++;
        });

        assert(comHash != 0);
        assert(comIndex < inside.size());
    }
};

template<class ItType, class ElemType>
ElemType indexOf(ItType& iterator, ElemType element)
{
    return std::distance(iterator.begin(),
                         std::find(iterator.begin(), iterator.end(), element));
}

template<class ItType, class ElemType>
bool exists(ItType& iterator, ElemType element)
{
    return std::find(iterator.begin(), iterator.end(), element)
                     != iterator.end();
}

template<class MapType>
int getNumOrbits(MapType& map)
{
    int count = 0;

    for (auto i = 0u; i < map.outside.size(); ++i)
    {
        auto innerIndex = i;
        while (map.inside[innerIndex] != map.comHash)
        {
            count++;
            innerIndex = indexOf(map.outside, map.inside[innerIndex]);
        }
        count++;
    }

    return count;
}

template<class MapType>
int getNumOrbitsToSanta(MapType& map)
{
    int count = 0;

    std::vector<std::size_t> visited;
    std::size_t intersection = 0;

    // step through from 'you' to 'com', noting each location
    {
        auto innerIndex = map.youIndex;
        while (map.inside[innerIndex] != map.comHash)
        {
            if (! exists(visited, map.inside[innerIndex]))
                visited.push_back(map.inside[innerIndex]);
            innerIndex = indexOf(map.outside, map.inside[innerIndex]);
        }
    }

    // count steps from 'san' to the intersection with the 'you' path
    {
        auto innerIndex = map.sanIndex;
        while (! exists(visited, map.inside[innerIndex]))
        {
            count++;
            innerIndex = indexOf(map.outside, map.inside[innerIndex]);
        }
        intersection = map.inside[innerIndex];
    }

    assert(intersection != 0);

    // add the number of steps from 'you' to the intersection
    count += indexOf(visited, intersection);

    return count;
}

int main()
{
    std::cout << "--- Day 6: Universal Orbit Map ---" << std::endl;

    constexpr std::array input = {
        "VHH)H4D"sv,"7Z7)2V2"sv,"9VL)WC5"sv,"D57)9QN"sv,"RXY)RGM"sv,"V5T)MBC"sv,"RQ1)LG8"sv,"SPC)B3F"sv,"TKR)W9Y"sv,"JCN)ZZB"sv,"PHT)7GD"sv,"GCD)TC7"sv,"15Y)DMX"sv,"XG2)TFF"sv,"TDY)GXF"sv,"3S7)G1J"sv,"KNK)BYQ"sv,"BS4)CQL"sv,"F2V)V32"sv,"7CL)QWL"sv,"Q83)YXK"sv,"QQ1)92P"sv,"MXL)BG9"sv,"7K7)L35"sv,"1TR)XL9"sv,"6TM)J59"sv,"DJR)J4T"sv,"M82)BMN"sv,"YHL)3F8"sv,"95G)HKS"sv,"DHT)3M4"sv,"K8Q)W54"sv,"XL9)M1S"sv,"5F8)FV8"sv,"BMN)PT7"sv,"HDX)9L6"sv,"KNG)2RT"sv,"RDS)C7T"sv,"YX5)195"sv,"SM6)23G"sv,"LS5)VTX"sv,"4W4)457"sv,"D5F)P9C"sv,"YJF)6XF"sv,"XBH)XFK"sv,"5P6)1TR"sv,"K8F)LHV"sv,"Q63)7LM"sv,"HWW)F6Y"sv,"4S4)WQK"sv,"YK6)67K"sv,"CYM)W46"sv,"9KJ)CKM"sv,"DXZ)PH9"sv,"S2G)KR9"sv,"B9B)DYP"sv,"WC5)RKV"sv,"NL9)N3C"sv,"513)HH8"sv,"VX7)DSQ"sv,"QH7)MVC"sv,"GV2)F6K"sv,"VYX)CLL"sv,"PDN)QPL"sv,"PGM)H3L"sv,"Y2M)DH9"sv,"DK4)QRV"sv,"6C2)KZS"sv,"BQZ)CG5"sv,"T79)6L4"sv,"CHV)TGK"sv,"QSK)H4K"sv,"K38)Y2M"sv,"1S8)6ZT"sv,"GYK)GN2"sv,"7YQ)P31"sv,"2RG)TQW"sv,"HH8)QJJ"sv,"887)Y7R"sv,"ZFL)LYL"sv,"GJH)NLP"sv,"3DB)GF2"sv,"PKG)JH4"sv,"PTW)6YH"sv,"FNR)Y92"sv,"9Q3)7YQ"sv,"LHT)ZW5"sv,"T17)DRN"sv,"GMX)R91"sv,"PMH)R8S"sv,"GN2)52Z"sv,"457)9VD"sv,"9XL)W4M"sv,"XV7)6GN"sv,"LG8)FXT"sv,"TFB)JMG"sv,"JZR)HDV"sv,"RG3)PKT"sv,"MG4)P5Y"sv,"HDN)KNG"sv,"WDK)CB4"sv,"NL6)887"sv,"FQQ)NLQ"sv,"P2X)81C"sv,"L2J)BBK"sv,"8KM)WW9"sv,"J7Z)P1Z"sv,"C52)JH1"sv,"YG4)NTX"sv,"LL6)YX5"sv,"3Q8)F98"sv,"MD8)3Q8"sv,"F6K)BGQ"sv,"3Q2)T1W"sv,"M2P)J7Z"sv,"38D)176"sv,"ZXQ)2YD"sv,"BT3)PC1"sv,"CWB)1Z9"sv,"DRX)SJD"sv,"M95)Z47"sv,"MQZ)37G"sv,"F6F)9VJ"sv,"1WS)M7K"sv,"WQ1)8C4"sv,"DMX)GC3"sv,"BSW)SF5"sv,"JSG)RG3"sv,"5TX)Z9P"sv,"NCX)X3G"sv,"QPL)JV3"sv,"1DD)D5S"sv,"ZM8)5K6"sv,"WBW)7JY"sv,"RVR)NCH"sv,"11L)LGF"sv,"QNP)CGY"sv,"WZ9)C2Z"sv,"WJG)FCR"sv,"VG9)FLR"sv,"RCT)PXM"sv,"SY2)FT6"sv,"PDH)63H"sv,"74B)VNM"sv,"4K2)919"sv,"BMW)2J6"sv,"T42)931"sv,"1G8)TNK"sv,"SYJ)WV9"sv,"97W)HG1"sv,"5K6)G89"sv,"VTB)XL3"sv,"1CK)7MN"sv,"19D)SRY"sv,"SYL)62P"sv,"BYQ)G9X"sv,"ZR3)HZQ"sv,"9CF)GVX"sv,"XVW)2CN"sv,"VZX)BZ6"sv,"WQK)34H"sv,"QCH)P49"sv,"KN6)VW3"sv,"G8T)7RH"sv,"2RR)5VJ"sv,"1KB)MHZ"sv,"4PQ)XBC"sv,"K1C)NB7"sv,"LGF)G9T"sv,"H3L)PNR"sv,"H4K)N3S"sv,"VNM)RVR"sv,"RXY)FVY"sv,"LL6)M5D"sv,"TK5)BPR"sv,"9R6)996"sv,"RC7)VWF"sv,"K2X)6QW"sv,"4VH)L7H"sv,"6RM)2NX"sv,"QZR)8FG"sv,"HX1)R5Z"sv,"RWL)DGX"sv,"KHX)RDQ"sv,"822)3FK"sv,"2TV)16M"sv,"QW4)7Z7"sv,"DZG)K5T"sv,"WHR)T7X"sv,"LB7)QDG"sv,"1JN)V55"sv,"Z27)XDQ"sv,"DMS)Q6C"sv,"9ZG)PZ2"sv,"DFV)F6F"sv,"SD1)8Y7"sv,"F3L)ZJ8"sv,"N86)YMP"sv,"SQX)QT3"sv,"9V5)FFW"sv,"X31)FKY"sv,"FKY)7SP"sv,"B3F)JKC"sv,"XGH)CN2"sv,"VML)8Y9"sv,"7K7)T7W"sv,"78F)92Q"sv,"BYF)QWC"sv,"DSQ)WFH"sv,"1P6)4VR"sv,"JV3)C77"sv,"6FB)GMX"sv,"XQ7)QQP"sv,"QHV)B2P"sv,"BS5)W7F"sv,"PXM)JTQ"sv,"PZG)JKS"sv,"PKX)61B"sv,"KJT)Q21"sv,"C3Y)9MJ"sv,"3X9)1PH"sv,"FV8)191"sv,"WP2)Y29"sv,"BDJ)G6V"sv,"4PT)NBL"sv,"FVY)PGH"sv,"6DH)LG4"sv,"NM4)335"sv,"84X)6WB"sv,"FG5)WXD"sv,"WW9)5T9"sv,"6HH)RC8"sv,"RKV)VYX"sv,"1QV)327"sv,"F26)H5Q"sv,"926)3QK"sv,"YKX)L7F"sv,"W7F)QB2"sv,"7FT)JRH"sv,"W2L)78F"sv,"LMS)68G"sv,"FG9)332"sv,"GWG)LF8"sv,"CDG)HRB"sv,"719)LW3"sv,"RX5)9YC"sv,"DRN)1CZ"sv,"4H9)XRS"sv,"RNR)D66"sv,"VWG)GSN"sv,"Q6C)7HR"sv,"34J)J8P"sv,"KRP)2KL"sv,"TT5)7GM"sv,"4JS)ZT4"sv,"R5S)JVN"sv,"TQW)4CG"sv,"9CP)HFC"sv,"F8S)VFR"sv,"143)MSK"sv,"MRM)PJD"sv,"7MN)RM8"sv,"BWL)B96"sv,"R7M)VMX"sv,"9PN)NZ6"sv,"B2N)1GQ"sv,"NCH)SG1"sv,"WSY)244"sv,"KDR)5GG"sv,"H3L)4S4"sv,"TGK)BMX"sv,"3FK)2RR"sv,"LW3)RDS"sv,"81T)47Y"sv,"S43)8WC"sv,"FHW)81D"sv,"3KY)M95"sv,"VMX)ZDP"sv,"VRQ)K4D"sv,"B2P)ZTW"sv,"367)8W1"sv,"ML8)TFB"sv,"XD5)Z5D"sv,"YHL)NNJ"sv,"KYC)3S4"sv,"KDR)5YG"sv,"5S3)6M2"sv,"QXW)3K9"sv,"67K)SM8"sv,"RHM)X5M"sv,"7SP)FDS"sv,"2RT)C6X"sv,"SGK)HHC"sv,"HL2)1QW"sv,"C77)TKR"sv,"D2F)8HX"sv,"SJD)9YT"sv,"TWT)X6L"sv,"Z8D)9MC"sv,"WFR)FBW"sv,"PQC)KY2"sv,"ZSC)VC3"sv,"WP6)TPQ"sv,"HKS)83C"sv,"KR9)6CD"sv,"YP5)J16"sv,"Y7R)KXM"sv,"X1K)71D"sv,"3GG)F7K"sv,"3H1)761"sv,"1GX)LB7"sv,"T99)HWW"sv,"YTG)JVQ"sv,"7XN)2S3"sv,"TP6)HX1"sv,"PJ1)8FH"sv,"MSK)4D9"sv,"F2V)6TM"sv,"GPG)HD8"sv,"ZJP)V8Q"sv,"Z3T)XKB"sv,"X25)CS5"sv,"H6N)X9F"sv,"WZZ)WBD"sv,"P3X)1GW"sv,"81D)K8L"sv,"H9Q)6W2"sv,"4HP)DHX"sv,"Y3K)NT5"sv,"FLR)B6M"sv,"4K2)SV5"sv,"JVN)XD5"sv,"6WP)KZH"sv,"MBC)X3D"sv,"3S3)MQP"sv,"KRJ)C2B"sv,"DB8)LLR"sv,"KZH)NJ8"sv,"X8D)3H1"sv,"C6X)MXL"sv,"4T1)999"sv,"BD1)8PY"sv,"HHK)S43"sv,"H4D)YT6"sv,"QH5)KRP"sv,"SRY)QCH"sv,"S5Z)B4W"sv,"87B)N51"sv,"G82)DNV"sv,"ZKV)DPP"sv,"GHL)PRJ"sv,"MYQ)G5R"sv,"1CZ)24L"sv,"6TS)BJ1"sv,"HRB)FJM"sv,"T3K)K8P"sv,"P9C)BS4"sv,"MTZ)B7R"sv,"NVB)6PS"sv,"Y3K)FP8"sv,"KWZ)771"sv,"999)2LL"sv,"J8P)RKY"sv,"6W2)BSB"sv,"WSY)MQZ"sv,"2GK)JYX"sv,"TTG)FT9"sv,"FKM)9WJ"sv,"JD1)RSH"sv,"CZ6)QP6"sv,"JFS)7T6"sv,"KZ9)KWH"sv,"V2J)BWL"sv,"XG3)L9Z"sv,"NGT)QTX"sv,"9L6)75P"sv,"BLK)P2V"sv,"9VD)JXZ"sv,"DPP)RV6"sv,"TC7)W7M"sv,"4LC)2BG"sv,"9ZL)TMT"sv,"TRW)CVN"sv,"XHJ)NL9"sv,"6PS)Q98"sv,"3BN)9DS"sv,"RV6)RX5"sv,"WWZ)J51"sv,"WW2)JZR"sv,"9MJ)X8J"sv,"5MK)XYD"sv,"BYR)H7Q"sv,"CVN)LDK"sv,"NZ6)8Z4"sv,"KKR)WN6"sv,"YX3)81T"sv,"GQ7)D35"sv,"K4D)XSH"sv,"ZT4)VWJ"sv,"GCQ)2D6"sv,"YBY)3LF"sv,"RDQ)ZDM"sv,"F7D)MTZ"sv,"H4D)FZJ"sv,"JR7)2KP"sv,"YQ8)H6H"sv,"C9X)7BK"sv,"81H)VP4"sv,"QQ1)3KY"sv,"HHC)P2X"sv,"CNJ)TCD"sv,"4ST)W4T"sv,"G5B)SYT"sv,"J92)J2P"sv,"MBB)N8W"sv,"HY5)XBR"sv,"5TS)B35"sv,"WV9)LG7"sv,"SN2)QNP"sv,"N5C)BJP"sv,"P1J)X81"sv,"JDR)PR6"sv,"NNJ)VX3"sv,"K8H)BSS"sv,"NHX)36R"sv,"BD2)HZT"sv,"YK6)8B8"sv,"D5Y)X7N"sv,"YV5)4JD"sv,"14L)W36"sv,"MZG)KNC"sv,"DMS)SGK"sv,"BZ6)MZP"sv,"WNG)6C1"sv,"DJL)GKQ"sv,"63W)2RL"sv,"MSP)C9X"sv,"Z12)WX8"sv,"75P)75F"sv,"T9Q)XNF"sv,"1LX)GS5"sv,"Z66)R6G"sv,"3FK)MRJ"sv,"ZD7)1P5"sv,"W36)X8L"sv,"KY2)VZX"sv,"667)H8H"sv,"CHV)TV1"sv,"TJ9)XQ7"sv,"BGQ)88G"sv,"NWG)Z8D"sv,"MVC)T7S"sv,"C9T)MNW"sv,"QR5)MZ3"sv,"YQ5)76Q"sv,"R5T)WMR"sv,"8MP)2SW"sv,"D66)KWZ"sv,"6GN)ZJV"sv,"CN5)WQ1"sv,"54L)XQG"sv,"G26)CM3"sv,"37T)9PN"sv,"JKS)49R"sv,"ZB9)KHC"sv,"X5M)BSW"sv,"NB7)QQF"sv,"8HX)HYJ"sv,"TTJ)SBX"sv,"G9X)VQV"sv,"67M)YPS"sv,"W54)DZ2"sv,"66K)5BW"sv,"D78)F96"sv,"SXM)PZG"sv,"FM2)VXB"sv,"VQV)GR7"sv,"FCR)WYD"sv,"T7W)C49"sv,"KJT)G26"sv,"2D6)JGB"sv,"5TN)LWV"sv,"YJW)TJ9"sv,"H5Q)671"sv,"F2X)ND7"sv,"VFR)1RP"sv,"B6D)SYJ"sv,"FBW)F3V"sv,"8Y9)Y3K"sv,"82B)KG7"sv,"8FN)LKP"sv,"LPS)3YY"sv,"JGF)RG9"sv,"H8L)PDV"sv,"JH1)3R6"sv,"1P5)B4P"sv,"S2G)ZX4"sv,"QQ9)FG5"sv,"3LF)3JY"sv,"1SZ)NG2"sv,"LSP)VQD"sv,"9WJ)Y4L"sv,"YCY)Z5C"sv,"J2S)6DP"sv,"6C6)4WK"sv,"JZR)3S3"sv,"H7Q)19N"sv,"GQJ)55P"sv,"YRF)JWB"sv,"HFT)F59"sv,"NX3)MBB"sv,"LLR)LWG"sv,"D7F)BYR"sv,"13Z)5Z2"sv,"VSD)6W3"sv,"Z1J)5TX"sv,"7D1)396"sv,"RR4)JDR"sv,"X64)P6Y"sv,"QWC)BH2"sv,"QT3)R95"sv,"QB2)LK2"sv,"L3M)866"sv,"Y7V)7CL"sv,"QRT)H19"sv,"NWW)SAN"sv,"6WW)75X"sv,"FHR)P8R"sv,"W4T)JHZ"sv,"G1J)XDG"sv,"G1K)3BN"sv,"QQF)1FB"sv,"JGT)L2J"sv,"4TF)LSP"sv,"XN3)4LC"sv,"RYH)F4C"sv,"PFQ)YT2"sv,"SQ9)YC3"sv,"CNV)DVP"sv,"C2Z)8V8"sv,"LCS)QVC"sv,"GF2)6LT"sv,"M1S)56W"sv,"47Y)SY2"sv,"V85)D7F"sv,"CM3)4T1"sv,"XZV)JD3"sv,"26V)5TQ"sv,"QBL)RSN"sv,"GV3)1MS"sv,"5BW)4MX"sv,"8Y7)PCQ"sv,"TQP)LS9"sv,"1KL)YYN"sv,"SSF)719"sv,"9VN)5KS"sv,"GJL)8X9"sv,"YMP)DHK"sv,"68G)XTD"sv,"761)7FT"sv,"T5F)2TP"sv,"W3G)67M"sv,"GSN)6MR"sv,"R33)363"sv,"H3Z)RYH"sv,"H1Y)JZP"sv,"BM1)RXY"sv,"8TL)QPY"sv,"RSH)MSP"sv,"LCQ)5K8"sv,"CHD)T2Q"sv,"JV3)MBW"sv,"NGT)TR3"sv,"G8B)SXW"sv,"T2Q)SSF"sv,"SF5)38D"sv,"J8P)8PJ"sv,"BKD)4B7"sv,"R7B)DZG"sv,"R84)YJT"sv,"5LZ)G5B"sv,"JHZ)J92"sv,"2SW)BM1"sv,"PT7)6HH"sv,"PC1)G8B"sv,"32C)NPM"sv,"V73)J9T"sv,"DGX)D78"sv,"4B7)FRL"sv,"QJX)QTF"sv,"PLV)ZDT"sv,"SG1)K2C"sv,"L16)H8L"sv,"LHV)XTS"sv,"1WF)6CP"sv,"QS6)J2S"sv,"H8H)H7L"sv,"HFC)Z58"sv,"WXF)WC1"sv,"1PH)VG5"sv,"D5S)ZNK"sv,"8S3)T17"sv,"Y2Z)HL2"sv,"48K)GY4"sv,"T7H)BKD"sv,"MHH)KZ9"sv,"Q4H)DRX"sv,"ZPD)TTG"sv,"B3F)H6N"sv,"6MR)KH5"sv,"ZGT)TRW"sv,"YPW)6RR"sv,"MZ3)LWT"sv,"PCQ)728"sv,"8PJ)KDR"sv,"FP3)VRZ"sv,"3LT)WFR"sv,"P6Y)2QT"sv,"BL5)C75"sv,"TTZ)DRY"sv,"YGP)DW5"sv,"7RH)6ZY"sv,"7RZ)557"sv,"J2P)GSQ"sv,"DB1)1GX"sv,"S5D)X3J"sv,"6RM)45B"sv,"JXZ)8WS"sv,"RGM)Y3P"sv,"2QT)B94"sv,"XKB)Z1J"sv,"3KK)SZF"sv,"LG7)ZGH"sv,"J51)QR5"sv,"T1R)B6D"sv,"TF9)RHM"sv,"2TV)1TS"sv,"W9Y)14L"sv,"CG5)C62"sv,"452)15Y"sv,"BG9)66K"sv,"V29)QSK"sv,"3NF)1SZ"sv,"3JS)5V2"sv,"TNK)QRT"sv,"YYN)RQ1"sv,"2TL)DB1"sv,"X8J)4KL"sv,"6DP)R53"sv,"Y3P)1JN"sv,"R7B)9KJ"sv,"4T8)158"sv,"73T)T2V"sv,"W7M)PYN"sv,"RDR)9Q3"sv,"VWF)7NH"sv,"MHZ)M57"sv,"2DD)QXW"sv,"M8V)FZ1"sv,"NLQ)T7H"sv,"6YH)PCF"sv,"VZD)ZJP"sv,"TLZ)LSG"sv,"YST)JCN"sv,"1GW)PQC"sv,"W46)RVW"sv,"1TS)8TT"sv,"58W)K8F"sv,"PDV)6C2"sv,"2LL)PDH"sv,"MFS)S2G"sv,"Q1Q)L8C"sv,"MQP)4T8"sv,"4Q7)VX7"sv,"D51)QSN"sv,"G5K)4TF"sv,"9VB)NPY"sv,"ZP7)Y8X"sv,"537)8J3"sv,"F1P)8JD"sv,"TV1)PTW"sv,"2YD)PNW"sv,"ZD2)NWG"sv,"15Y)QDY"sv,"BLW)FKM"sv,"V8H)GHL"sv,"7VY)BLK"sv,"WW2)L4J"sv,"3F8)98R"sv,"KGR)D1M"sv,"5RF)WPK"sv,"J16)YMR"sv,"R8W)WWZ"sv,"CXC)8HN"sv,"K1Z)F1F"sv,"5TQ)8CK"sv,"3S7)T5F"sv,"6VM)JWP"sv,"XCC)CNV"sv,"YXK)X75"sv,"Q6C)R7B"sv,"MNW)G39"sv,"5K8)2JS"sv,"WPB)ZD7"sv,"P49)8TL"sv,"H7L)CHJ"sv,"BCT)FX2"sv,"V32)3LT"sv,"3R6)B9B"sv,"CH2)KRJ"sv,"3KK)JSG"sv,"M35)Q48"sv,"C6C)NQN"sv,"HG4)BLS"sv,"K2C)P99"sv,"7MJ)MQY"sv,"TNX)16H"sv,"PZ2)F26"sv,"6CP)X8D"sv,"2GH)4M5"sv,"J6M)H3V"sv,"QL5)P1J"sv,"KXJ)JLJ"sv,"VPZ)D5F"sv,"4GX)KNK"sv,"CV9)KL9"sv,"K8L)76C"sv,"W9Y)X7L"sv,"ZQK)13Z"sv,"Z5C)HXM"sv,"X3J)T3B"sv,"B75)BR6"sv,"189)PTR"sv,"RG9)T6C"sv,"MD8)4ST"sv,"D35)GZZ"sv,"Z9P)LWW"sv,"XL3)W3G"sv,"QXW)3RZ"sv,"SZF)1TF"sv,"ZTW)63W"sv,"37G)WXT"sv,"XP6)YTG"sv,"MGZ)3D6"sv,"LG1)XBH"sv,"XPW)5F5"sv,"ZDM)G8N"sv,"WMR)P3G"sv,"KWH)HQW"sv,"CSH)R7M"sv,"919)6TS"sv,"9YT)BYF"sv,"XYQ)HVX"sv,"41B)TXP"sv,"HLQ)1G8"sv,"PBK)B64"sv,"4FC)DFV"sv,"X15)Q4H"sv,"Q98)CBH"sv,"61B)HX4"sv,"JLJ)D51"sv,"NB7)4HG"sv,"F18)CN5"sv,"16M)1BN"sv,"VRC)FL6"sv,"DK3)6VM"sv,"TMT)H1Y"sv,"KZN)XFC"sv,"CJ1)CH2"sv,"B35)PKX"sv,"73R)PGM"sv,"D5F)423"sv,"5LK)G8T"sv,"6WB)M9J"sv,"H2P)C7W"sv,"H3V)QLN"sv,"YZ6)KKR"sv,"PSN)ZYM"sv,"F6K)CTK"sv,"TDY)QD5"sv,"JRH)5TS"sv,"NPM)JC1"sv,"28L)QW4"sv,"HZT)87B"sv,"D1M)CZ6"sv,"8C4)MD8"sv,"8PG)1LX"sv,"JTZ)T7B"sv,"JD3)SMC"sv,"T2V)TW7"sv,"KGF)K8Q"sv,"L91)ZRN"sv,"Z5D)2N3"sv,"KS7)SD1"sv,"512)D5C"sv,"W29)YHL"sv,"S65)HS9"sv,"XBC)9T2"sv,"C7T)GWG"sv,"C49)4FC"sv,"RLW)YQ5"sv,"Z58)Z5B"sv,"VLS)MK1"sv,"R1X)3MM"sv,"2WP)KCY"sv,"SN3)JP1"sv,"JVQ)HNP"sv,"QLN)4K2"sv,"5F5)7FN"sv,"V8Q)H2P"sv,"4S5)ML8"sv,"6C6)34J"sv,"JKC)V29"sv,"QRV)ZHP"sv,"BG1)P65"sv,"51Q)8PG"sv,"CLL)VHH"sv,"HG1)LS3"sv,"K8F)KR2"sv,"Y7B)W29"sv,"JR9)D1L"sv,"ZZB)ZLL"sv,"95W)9XX"sv,"R53)S98"sv,"BRC)C6C"sv,"MKH)QQQ"sv,"MC9)WNG"sv,"BTF)R1X"sv,"C7W)R84"sv,"KLC)KFF"sv,"L9Z)DFJ"sv,"C62)Q9J"sv,"GR7)86R"sv,"VKB)PMH"sv,"RM8)TP6"sv,"TTB)SJP"sv,"G5K)VD1"sv,"PJB)K2X"sv,"XV8)RNR"sv,"5YT)TT5"sv,"J32)5TN"sv,"YJT)FNR"sv,"MKC)95W"sv,"R82)WW2"sv,"C96)FRC"sv,"Z19)WBW"sv,"PNR)N86"sv,"DYQ)KDX"sv,"FZ1)RR4"sv,"363)S38"sv,"5Z2)V73"sv,"NSZ)X97"sv,"9DS)5PL"sv,"D5S)L16"sv,"LYL)73X"sv,"BSS)GML"sv,"L35)JGL"sv,"G8N)SQ9"sv,"GJH)189"sv,"BW5)B75"sv,"8J3)5G3"sv,"RC8)W2L"sv,"5G3)TNX"sv,"K8P)SN2"sv,"3S4)PT5"sv,"XFC)QVY"sv,"8TP)P78"sv,"VYF)JK4"sv,"6YV)LGY"sv,"GKQ)NM4"sv,"WXD)RC7"sv,"HR4)PFQ"sv,"JYY)5NX"sv,"JH4)C96"sv,"R2Q)5C6"sv,"N7H)RNW"sv,"55P)D5Y"sv,"GC1)1KL"sv,"4YX)382"sv,"V63)XZV"sv,"XSK)5YL"sv,"TQT)12Y"sv,"Q9J)9R6"sv,"X97)4CY"sv,"FDG)8S4"sv,"NM4)TGG"sv,"244)54L"sv,"VLH)M82"sv,"VP9)537"sv,"JTQ)YWW"sv,"19N)N3J"sv,"QVC)5CG"sv,"2LZ)2SK"sv,"98R)FP3"sv,"FL6)VNK"sv,"PTW)81H"sv,"KFF)8MP"sv,"N9T)XN3"sv,"FX2)82Q"sv,"8W8)951"sv,"KYC)KLN"sv,"PFQ)JBS"sv,"N3S)SM6"sv,"QPT)KGR"sv,"YPS)F9M"sv,"PT5)GKY"sv,"RVW)9M2"sv,"2TP)JLP"sv,"MRJ)XG2"sv,"F96)2LZ"sv,"2JS)LS5"sv,"8CK)48K"sv,"ZTW)DJR"sv,"BSD)5NF"sv,"LRY)BT5"sv,"P31)7SK"sv,"2HT)B6V"sv,"5PL)BD1"sv,"5G1)HBX"sv,"JZM)FWT"sv,"KG7)ZSC"sv,"WYD)MGZ"sv,"F6J)KL5"sv,"HDV)NSZ"sv,"LX6)B7J"sv,"BMX)VTB"sv,"GSQ)PBK"sv,"71D)Z3T"sv,"7JY)NHX"sv,"9XX)37T"sv,"8S4)19D"sv,"RC2)HFT"sv,"ZYM)F1P"sv,"B7J)DG2"sv,"78R)PHV"sv,"P8R)BH1"sv,"RGM)YJW"sv,"WPB)XQ2"sv,"7GD)WRW"sv,"NGQ)B2N"sv,"SKS)R5T"sv,"BT5)9KC"sv,"JFS)MKC"sv,"M3J)JN1"sv,"QTX)S9Q"sv,"YC3)3VJ"sv,"Y3M)JZM"sv,"TCK)RWF"sv,"3QK)ZB9"sv,"WFH)CXC"sv,"RYH)Y3Q"sv,"NNB)G8H"sv,"SV5)SL2"sv,"JBS)T9Q"sv,"SX9)YK6"sv,"MZP)MG4"sv,"4CG)3JS"sv,"XKP)MC9"sv,"DVP)SYL"sv,"BBK)N2F"sv,"D9R)KD2"sv,"CGY)S8Z"sv,"VYZ)7CG"sv,"SJP)9VN"sv,"ZJ8)3NF"sv,"68G)2X3"sv,"B7J)LHT"sv,"TFP)YZ6"sv,"DZ2)5MK"sv,"RR4)23R"sv,"DF6)QFS"sv,"3NN)GPG"sv,"LWG)7NV"sv,"YQQ)VSD"sv,"VXB)Q29"sv,"Q29)QLF"sv,"N3J)KGF"sv,"G89)YRF"sv,"NCR)VYF"sv,"BPJ)NGX"sv,"L7H)R2Q"sv,"N5J)1DD"sv,"PNP)KN6"sv,"SSF)2HT"sv,"2KP)4W4"sv,"X8J)GV3"sv,"TFF)1NV"sv,"7T6)GC1"sv,"F2Y)K38"sv,"SSD)2GK"sv,"QTD)YLN"sv,"557)CSH"sv,"216)2DD"sv,"SBV)NW3"sv,"83C)QQ1"sv,"52Z)SSD"sv,"H1Y)DK3"sv,"RSN)73T"sv,"YT6)JFS"sv,"HCB)8TP"sv,"QDG)BXM"sv,"4HG)H3Z"sv,"SYT)HYH"sv,"D5C)PNV"sv,"5TM)9V5"sv,"X8D)WSY"sv,"ZNK)4JS"sv,"85R)6FB"sv,"JLP)HF9"sv,"PTX)26V"sv,"4M5)DB8"sv,"9QN)7MJ"sv,"GS5)6DH"sv,"C8M)ZR3"sv,"24L)7K7"sv,"SKS)LKF"sv,"KH5)VRQ"sv,"R5Z)NVR"sv,"1Z9)HR6"sv,"LKF)1D6"sv,"K38)VPZ"sv,"8JD)W1M"sv,"GVX)YV5"sv,"T5H)KTP"sv,"T3B)41B"sv,"SXW)TS7"sv,"XTS)SN3"sv,"NRX)5G1"sv,"1W2)HDX"sv,"9VD)VRC"sv,"B9D)XGH"sv,"V55)QJX"sv,"VRZ)CDC"sv,"QFS)X64"sv,"RL4)TMP"sv,"J2P)Y7V"sv,"671)GYK"sv,"B7R)LX6"sv,"M5D)J32"sv,"RVY)7RZ"sv,"DRY)1W2"sv,"YWF)XVH"sv,"KXM)F18"sv,"J6M)Y3M"sv,"7PH)Q1Q"sv,"5WS)Z38"sv,"VWG)ZXQ"sv,"H4K)HR4"sv,"BPR)PDN"sv,"728)NX5"sv,"NBL)ZM8"sv,"CB4)MGJ"sv,"V9G)PLV"sv,"SMC)555"sv,"BH2)W84"sv,"23R)Q83"sv,"XJ1)HDW"sv,"B72)YQQ"sv,"BTR)12J"sv,"VHX)3GG"sv,"ZX4)SBV"sv,"B6M)CR2"sv,"BMS)D4C"sv,"TW7)9TH"sv,"PDW)RDR"sv,"5NF)GJH"sv,"HH8)51Q"sv,"332)1QV"sv,"K2L)HQR"sv,"DRY)G82"sv,"NLP)M3J"sv,"Z2G)72V"sv,"QNH)B9D"sv,"HP3)3NN"sv,"9ZL)G5K"sv,"F6Y)2GH"sv,"4VR)DHT"sv,"CD7)G3W"sv,"QCH)QHV"sv,"LGF)3Z6"sv,"23G)3GY"sv,"7NH)N5C"sv,"LBC)SX9"sv,"S38)6WW"sv,"C2B)2WP"sv,"RPS)V85"sv,"P1Z)TWT"sv,"WRW)WHR"sv,"45B)TTB"sv,"N2F)2RG"sv,"2SQ)NPV"sv,"HZQ)VKW"sv,"ZHP)SHF"sv,"8WC)VZD"sv,"PYN)HLL"sv,"82Q)KXJ"sv,"X8L)3JC"sv,"5MK)PNP"sv,"VWJ)Z66"sv,"996)C52"sv,"SBX)MZG"sv,"6H9)YPW"sv,"LS9)HDN"sv,"NPV)QNH"sv,"X7N)BMS"sv,"BLK)DF6"sv,"GXF)ZKV"sv,"XNT)XP6"sv,"KLN)VDL"sv,"TTB)TF9"sv,"WC1)CD7"sv,"5HL)Z19"sv,"F1F)6H9"sv,"X9F)VKB"sv,"ZW5)YWF"sv,"MBW)XYQ"sv,"8WS)JYY"sv,"BR6)KS7"sv,"XFK)M2P"sv,"XQG)F2X"sv,"X81)7VY"sv,"FHZ)VBG"sv,"7CG)LBC"sv,"YBT)JW6"sv,"FRL)82B"sv,"VS3)7D1"sv,"S8Z)SXM"sv,"TPQ)3NV"sv,"B64)R5S"sv,"2BG)RVY"sv,"7SK)XJ1"sv,"771)C9T"sv,"ZDT)CQ3"sv,"C62)C4Z"sv,"RT8)WP2"sv,"B9Z)NVB"sv,"9QN)QZR"sv,"TGG)5X6"sv,"ZRN)3GN"sv,"4D9)2DJ"sv,"XNF)K5H"sv,"6XF)VML"sv,"G2V)5LZ"sv,"WYD)74B"sv,"P5Y)T42"sv,"8FG)DMS"sv,"PNL)8W8"sv,"435)5F8"sv,"3RZ)1S8"sv,"KLC)V2J"sv,"PYN)5HL"sv,"5V2)32C"sv,"1QW)KX1"sv,"1D6)78R"sv,"76C)8WW"sv,"4WK)M13"sv,"CQL)XHJ"sv,"297)2SQ"sv,"F3V)9DC"sv,"T2Q)SDL"sv,"1BN)SBP"sv,"TZB)9QR"sv,"W4V)T1T"sv,"81C)VG9"sv,"J9T)49L"sv,"27L)NGQ"sv,"JP1)3K5"sv,"DHK)MHH"sv,"M13)4VH"sv,"XYH)4GX"sv,"7Q3)GCQ"sv,"423)PJ1"sv,"1TF)F6J"sv,"NTX)LJJ"sv,"RNW)FDG"sv,"951)W1W"sv,"HQW)FG9"sv,"6M2)F1B"sv,"34Y)3SR"sv,"9MC)XV7"sv,"3YH)JZ1"sv,"M9J)ST7"sv,"LF8)PJB"sv,"SHF)ZHS"sv,"9QR)KZN"sv,"X1K)35X"sv,"JYX)BG1"sv,"QVY)G2N"sv,"FJM)LMS"sv,"DHX)TZB"sv,"TCD)M8V"sv,"WXT)T3K"sv,"C8V)8FN"sv,"CS5)ZPD"sv,"191)L3M"sv,"3MM)HLQ"sv,"KZS)K1C"sv,"FZJ)BVC"sv,"8WH)QDC"sv,"VC3)TK5"sv,"35X)C8M"sv,"G8H)Z5P"sv,"3WZ)7Q3"sv,"QSL)XSK"sv,"XRS)DK4"sv,"JZP)DST"sv,"L7F)84X"sv,"K5T)GQ7"sv,"NW4)ZQK"sv,"3GY)BLJ"sv,"2DB)NB9"sv,"H19)3DB"sv,"M57)WHG"sv,"LSG)926"sv,"SM6)MQH"sv,"Y4L)KHX"sv,"VTX)BQZ"sv,"2X3)Y2R"sv,"51Q)BPJ"sv,"2N2)3KK"sv,"ND7)VHX"sv,"NX5)5RF"sv,"ZLL)G2V"sv,"H4V)MKH"sv,"73T)435"sv,"LGD)W4V"sv,"2KL)WPB"sv,"CDM)NCX"sv,"513)VLS"sv,"JW6)BTR"sv,"KHC)8R5"sv,"YN2)NCR"sv,"62P)667"sv,"J92)3X9"sv,"8FH)HHK"sv,"KR2)SZX"sv,"G8B)VP9"sv,"73X)QSQ"sv,"1RP)R2H"sv,"7MJ)7W3"sv,"QWL)4YX"sv,"WL2)5MD"sv,"5L8)18G"sv,"HBX)NSF"sv,"Z73)4P1"sv,"9HZ)YZV"sv,"42L)YKX"sv,"T5F)KJT"sv,"7RN)QS6"sv,"7HS)BL5"sv,"7HR)7XN"sv,"Y2R)SPC"sv,"ZM7)G1K"sv,"MQH)X1K"sv,"KDX)YBT"sv,"KL9)3BZ"sv,"Y3Q)9ZL"sv,"DST)D9R"sv,"TS7)YOU"sv,"LWV)RLW"sv,"GML)367"sv,"2VB)V34"sv,"WN6)6RM"sv,"195)VWQ"sv,"23C)9VB"sv,"FFW)R33"sv,"S98)B9Z"sv,"B4P)YG4"sv,"QSK)BS5"sv,"XBR)3C2"sv,"49R)DLH"sv,"Z38)LRY"sv,"8Y7)4Q7"sv,"5VJ)CHD"sv,"HVX)7HS"sv,"LGY)SQX"sv,"3K5)MYQ"sv,"T3B)N5J"sv,"R95)69G"sv,"2CN)WP6"sv,"VBG)TCK"sv,"V34)NLB"sv,"39B)QBL"sv,"YWW)27L"sv,"SBP)YJF"sv,"PKT)PXR"sv,"2GV)T5H"sv,"4KL)XYH"sv,"BKV)FHR"sv,"VNK)H9Q"sv,"3GN)2GV"sv,"JN1)GQJ"sv,"J59)BMY"sv,"QL5)CHV"sv,"3NV)LPS"sv,"FJM)GM8"sv,"PTR)6WP"sv,"DF1)58W"sv,"7LM)XG3"sv,"DJR)CLY"sv,"PJD)S73"sv,"12J)LCQ"sv,"P2V)RQC"sv,"XTD)F8S"sv,"VZX)CNJ"sv,"L9W)CNL"sv,"XDG)QPT"sv,"86R)6YV"sv,"FDS)RC2"sv,"HDW)JGT"sv,"ZDP)CDM"sv,"LK2)YX3"sv,"HR6)T79"sv,"9YC)4Y7"sv,"X7L)V63"sv,"Z5P)B72"sv,"NPY)TQP"sv,"W4M)TNP"sv,"VMR)GJL"sv,"HYJ)NWW"sv,"ZJV)HCB"sv,"76Q)77Q"sv,"N89)Y2Z"sv,"5NX)78B"sv,"SRJ)Q28"sv,"TR3)4HP"sv,"T79)5L8"sv,"JBS)BKV"sv,"3SR)C8V"sv,"ZT8)T99"sv,"NQN)8QP"sv,"7R8)NL6"sv,"CSH)73R"sv,"FDG)JSS"sv,"435)LCS"sv,"RKY)PKG"sv,"HYH)GCD"sv,"T1T)FQQ"sv,"ZNK)K8H"sv,"KL5)BLW"sv,"TXP)K1Z"sv,"NSF)CV9"sv,"12Y)CDG"sv,"78B)YN2"sv,"HF9)XCC"sv,"NGX)6C6"sv,"YX5)LGD"sv,"XYD)T2P"sv,"RK4)YCY"sv,"CBH)FM2"sv,"BVC)2TL"sv,"LHT)FHZ"sv,"3YY)2TV"sv,"CHJ)DF1"sv,"VX3)1P6"sv,"8R5)FVB"sv,"18G)7R8"sv,"CLY)5P6"sv,"BR4)NW4"sv,"TNK)512"sv,"SDL)V5T"sv,"JCN)DJL"sv,"C75)DK1"sv,"WWR)BW5"sv,"GM8)KYC"sv,"8B8)QL5"sv,"FVB)143"sv,"6C1)S65"sv,"Q21)BTF"sv,"VW3)MRM"sv,"3S3)N89"sv,"HHK)QQ9"sv,"3BZ)S5D"sv,"6RR)2DB"sv,"452)42L"sv,"8TT)3WZ"sv,"7GM)JR9"sv,"N51)MQC"sv,"T2P)CWB"sv,"HQR)WZ9"sv,"X75)WL2"sv,"9T2)N7H"sv,"T1W)TQT"sv,"5TX)X15"sv,"PR6)ZGT"sv,"36R)VMR"sv,"X3D)X31"sv,"PNV)5S3"sv,"92Q)34Y"sv,"X74)3S7"sv,"6G3)P3X"sv,"858)T89"sv,"H8H)XKP"sv,"DK1)MFS"sv,"NJ8)LVJ"sv,"5X6)216"sv,"TNP)2N2"sv,"VD1)WDK"sv,"DG2)Q63"sv,"QQP)95G"sv,"VZ2)Z12"sv,"Z5B)RL4"sv,"4MX)F2Y"sv,"S9Q)NX3"sv,"DY8)YST"sv,"3VJ)RT8"sv,"MGZ)BCT"sv,"MQY)QVJ"sv,"X3G)JW1"sv,"8QP)858"sv,"PGH)BD2"sv,"GXF)K2L"sv,"JK4)95C"sv,"6ZY)VS3"sv,"P99)BR4"sv,"1DD)PDW"sv,"BLS)8S3"sv,"2GH)SRJ"sv,"DYP)581"sv,"LDK)JR7"sv,"VWQ)J6M"sv,"4JD)6P1"sv,"CN2)7RN"sv,"X5M)F2V"sv,"N8W)Y7B"sv,"B6V)QH5"sv,"JWP)F3L"sv,"H6H)CDV"sv,"HD8)XV8"sv,"BJ1)PSN"sv,"Q28)PBC"sv,"437)BRC"sv,"NNB)T1R"sv,"HLL)Z2G"sv,"F5S)TTZ"sv,"JSS)TLZ"sv,"YQ5)3SC"sv,"92P)HG4"sv,"HLQ)TTJ"sv,"49L)4PT"sv,"PQC)CJ1"sv,"BJ1)ZT8"sv,"G39)S5Z"sv,"F7K)73H"sv,"KX1)QSL"sv,"QD5)L91"sv,"HXM)BMW"sv,"QTF)YBY"sv,"V5T)Z73"sv,"3D6)WWR"sv,"8TT)1KB"sv,"VG5)4PQ"sv,"95C)297"sv,"K5H)V9G"sv,"BMY)QTD"sv,"JC1)RPS"sv,"PH9)X25"sv,"QPY)BT3"sv,"YLN)YQ8"sv,"YZV)NRX"sv,"P65)3DL"sv,"LK2)F5S"sv,"DLH)KLC"sv,"COM)XVW"sv,"QP6)5TM"sv,"WHG)DY8"sv,"Y29)DYQ"sv,"2RL)PHT"sv,"MZ3)SKS"sv,"2V2)YP5"sv,"CDC)5WS"sv,"QVJ)JGF"sv,"X6L)D2F"sv,"LJJ)7PH"sv,"T7X)TFP"sv,"FXT)LG1"sv,"VKW)3YH"sv,"34H)8WH"sv,"1NV)QP2"sv,"JWB)513"sv,"8HN)BSD"sv,"QDY)9CF"sv,"GY4)2VB"sv,"931)Z27"sv,"B96)9XL"sv,"CYM)437"sv,"382)WXF"sv,"23C)RCT"sv,"FHZ)85R"sv,"56W)Q3C"sv,"VW1)R82"sv,"72V)C3Y"sv,"VRZ)V3G"sv,"FRC)QH7"sv,"LKP)LL6"sv,"F4C)H4V"sv,"WPK)VWG"sv,"7NV)VWW"sv,"YMR)1WF"sv,"PCF)NNB"sv,"FP8)VYZ"sv,"DNV)TDY"sv,"ZFL)F7D"sv,"6CD)ZD2"sv,"CKM)8KM"sv,"P78)GV2"sv,"QJJ)28L"sv,"PDH)3Q2"sv,"G5R)BDJ"sv,"CTK)6G3"sv,"Z5D)9ZG"sv,"C4Z)5LK"sv,"LS3)ZP7"sv,"QLF)23C"sv,"1FB)452"sv,"SZX)1CK"sv,"NW3)V8H"sv,"Q21)XNT"sv,"G2Y)5YT"sv,"MK1)RK4"sv,"DH9)JD1"sv,"C4Z)N9T"sv,"7BK)2KF"sv,"PXR)WZZ"sv,"DW5)Q43"sv,"6P1)XPW"sv,"T7B)FHW"sv,"X3D)PTX"sv,"CNL)1WS"sv,"QDC)ZFL"sv,"335)ZM7"sv,"557)NGT"sv,"YT2)CYM"sv,"MQC)PNL"sv,"SN3)WJG"sv,"Y7B)53N"sv,"RWF)L9W"sv,"5YG)G2Y"sv,"6W3)822"sv,"77Q)DXZ"sv,"6L4)HP3"sv,"S9Q)9HZ"sv,"3Z6)39B"sv,"VDL)JTZ"sv,"158)M35"sv,"GZZ)11L"sv,"BVC)RWL"sv,"88G)R8W"sv,"P3G)HY5"sv,"8X9)X74"sv,"5CG)9CP"sv,"6ZT)4S5"sv,"F98)97W"sv,"VQD)VW1"sv,"KNC)YGP"sv,"CWB)4H9"sv,"2HT)D57"sv,"N3C)9VL"sv,"FWT)VZ2"sv,"QP6)PFX"sv,"LVJ)VLH"sv
    };

    constexpr std::array testInput = {
        "COM)B"sv,"B)C"sv,"C)D"sv,"D)E"sv,"E)F"sv,"B)G"sv,"G)H"sv,"D)I"sv,"E)J"sv,"J)K"sv,"K)L"sv
    };

    constexpr std::array testInput2 = {
        "COM)B"sv,"B)C"sv,"C)D"sv,"D)E"sv,"E)F"sv,"B)G"sv,"G)H"sv,"D)I"sv,"E)J"sv,"J)K"sv,"K)L"sv,"K)YOU"sv,"I)SAN"sv,
    };

    auto testMap = Map(testInput);
    assert(getNumOrbits(testMap) == 42);

    auto testMap2 = Map(testInput2);
    assert(getNumOrbitsToSanta(testMap2) == 4);

    auto InputMap = Map(input);
    std::cout << "Part 1: " << getNumOrbits(InputMap) << std::endl;
    std::cout << "Part 2: " << getNumOrbitsToSanta(InputMap) << std::endl;

    return 0;
}
