# debug build:
#   nim c -r day15.nim -opt:speed

# benchmark:
#   timeit day15.nim -d:release -opt:speed

import tables

const puzzleInput = [2,1,10,11,0,6]

const testInput = [
    ([0,3,6], 10, 0),
    ([0,3,6], 2020, 436),
    ([1,3,2], 2020, 1),
    ([2,1,3], 2020, 10),
    ([1,2,3], 2020, 27),
    ([2,3,1], 2020, 78),
    ([3,2,1], 2020, 438),
    ([3,1,2], 2020, 1836)
]

proc spokenAt(input: openArray[int], position: Natural): int32 =
    var
        spoken: Table[int32, int32]
        turn = input.len.int32
        prev = input[^1].int32

    for i, val in input[0..^2].pairs:
        spoken[val.int32] = i.int32 + 1
    
    while turn < position:
        let index = spoken.getOrDefault(prev, 0'i32)
        spoken[prev] = turn
        prev = if index == 0: index else: turn - index
        turn.inc
    prev

# tests
for test in testInput:
    assert spokenAt(test[0], test[1]) == test[2]

let part1Answer = spokenAt(puzzleInput, 2020)
let part2Answer = spokenAt(puzzleInput, 30_000_000)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 232
assert part2Answer == 18929178
