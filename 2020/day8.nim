# debug build:
#   nim c -r day8.nim -opt:speed

# benchmark:
#   timeit day8.nim -d:release -opt:speed

import strutils, sequtils

const puzzleInput = readFile("day8.txt").splitLines()

const testInput = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6""".splitlines()

proc executeLine(line: string, pc, acc: var int) =
    let split = line.split()
    let op = split[0]
    let val = split[1]
    
    case op:
        of "acc":
            acc += parseInt(val)
            pc.inc
        of "jmp":
            pc += parseInt(val)
        of "nop":
            pc.inc
        else:
            assert false, "unknown instruction"

proc valueAtLoop(input: openArray[string]): int =
    var lineExecuted: seq[int]
    var acc, pc: int

    while pc < input.len:
        lineExecuted.add(pc)
        executeLine(input[pc], pc, acc)
        if pc in lineExecuted:
            break

    echo acc
    acc

proc run(input: openArray[string], acc: var int): bool =
    var lineExecuted: seq[int]
    var pc: int
    acc = 0

    while pc < input.len:
        lineExecuted.add(pc)
        executeLine(input[pc], pc, acc)
        if pc in lineExecuted:
            break

    pc == input.len

proc valueAtEnd(input: openArray[string]): int =
    for i in 0..<input.len:
        var program = input.toSeq
        if "jmp" in program[i]:
            program[i] = program[i].replace("jmp", "nop")
        elif "nop" in program[i]:
            program[i] = program[i].replace("nop", "jmp")
        else:
            continue
        if run(program, result):
            break

# tests
assert valueAtLoop(testInput) == 5
assert valueAtEnd(testInput) == 8

const part1Answer = valueAtLoop(puzzleInput)
const part2Answer = valueAtEnd(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 1723
assert part2Answer == 846
