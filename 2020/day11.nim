# debug build:
#   nim c -r day11.nim -opt:speed

# benchmark:
#   timeit day11.nim -d:release -opt:speed

import strutils, sequtils

const puzzleInput = readFile("day11.txt").replace('L', '#').splitLines

const testInput =
    ("""L.LL.LL.LL
    LLLLLLL.LL
    L.L.L..L..
    LLLL.LL.LL
    L.LL.LL.LL
    L.LLLLL.LL
    ..L.L.....
    LLLLLLLLLL
    L.LLLLLL.L
    L.LLLLL.LL""".unindent.replace('L', '#').splitLines, 37, 26)

const
    occ = '#'
    emp = 'L'
    flr = '.'

iterator grid(maxRows, maxCols: int): tuple[x: int, y: int] =
    for row in 0..maxRows:
        for col in 0..maxCols:
            yield (row, col)

iterator surround(origin: tuple[x, y: int], maxX, maxY: int): tuple[x, y: int] =
    let
        x = origin.x
        y = origin.y
    var
        chkdLeft, chkdRight, chkdUp, chkdDown, chkdTLeft, chkdTRight, chkdBLeft, chkdBRight: bool
    if (not chkdLeft) and x - 1 >= 0:
        chkdLeft = true
        yield (x - 1, y)
    if (not chkdRight) and x + 1 <= maxX:
        chkdRight = true
        yield (x + 1, y)
    if (not chkdUp) and y - 1 >= 0:
        chkdUp = true
        yield (x, y - 1)
    if (not chkdDown) and y + 1 <= maxY:
        chkdDown = true
        yield (x, y + 1)
    if (not chkdTLeft) and (x - 1 >= 0) and (y - 1 >= 0):
        chkdTLeft = true
        yield (x - 1, y - 1)
    if (not chkdTRight) and (x + 1 <= maxX) and (y - 1 >= 0):
        chkdTRight = true
        yield (x + 1, y - 1)
    if (not chkdBLeft) and (x - 1 >= 0) and (y + 1 <= maxY):
        chkdBLeft = true
        yield (x - 1, y + 1)
    if (not chkdBRight) and (x + 1 <= maxX) and (y + 1 <= maxY):
        chkdBRight = true
        yield (x + 1, y + 1)

proc numOccupiedSeats(input: openArray[string]): int =
    var grid: seq[seq[char]]

    for line in input:
        grid.add(cast[seq[char]](line))

    let maxX = input[0].high
    let maxY = input.high

    var changeMade: int

    while true:
        changeMade = 0
        var gridCopy = grid
        for x, y in grid(maxX, maxY):
            var surr: string
            let current = grid[y][x]
            if current == flr:
                # no one sits on the floor
                continue
            if x - 1 >= 0:
                # left
                surr.add(grid[y][(x - 1)])
            if x + 1 <= maxX:
                # right
                surr.add(grid[y][x + 1])
            if y - 1 >= 0:
                # up
                surr.add(grid[y - 1][x])
            if y + 1 <= maxY:
                # down
                surr.add(grid[y + 1][x])
            if (x - 1 >= 0) and (y - 1 >= 0):
                # top left
                surr.add(grid[y - 1][x - 1])
            if (x + 1 <= maxX) and (y - 1 >= 0):
                # top right
                surr.add(grid[y - 1][x + 1])
            if (x - 1 >= 0) and (y + 1 <= maxY):
                # bottom left
                surr.add(grid[y + 1][x - 1])
            if (x + 1 <= maxX) and (y + 1 <= maxY):
                # bottom right
                surr.add(grid[y + 1][x + 1])
            # for xi, yi in surround((x, y), maxX, maxY):
            #     surr.add(grid[y][x])

            let numSeats = surr.countIt(it == occ)
            if current == emp and numSeats == 0:
                gridCopy[y][x] = occ
                changeMade.inc
            elif current == occ and numSeats >= 4:
                gridCopy[y][x] = emp
                changeMade.inc
        if changeMade == 0:
            break
        else:
            grid = gridCopy
    
    for x, y in grid(maxX, maxY):
        if grid[y][x] == occ:
            result.inc

iterator up(start: tuple[x: int, y: int], minY: int = 0): tuple[x: int, y: int] =
    if start.y > 0:
        for y in countDown(start.y - 1, minY):
            yield (start.x, y)

iterator down(start: tuple[x: int, y: int], maxY: int): tuple[x: int, y: int] =
    if start.y < maxY:
        for y in countUp(start.y + 1, maxY):
            yield (start.x, y)

iterator left(start: tuple[x: int, y: int], minX: int = 0): tuple[x: int, y: int] =
    if start.x > 0:
        for x in countDown(start.x - 1, minX):
            yield (x, start.y)

iterator right(start: tuple[x: int, y: int], maxX: int): tuple[x: int, y: int] =
    if start.x < maxX:
        for x in countUp(start.x + 1, maxX):
            yield (x, start.y)

iterator topLeft(start: tuple[x: int, y: int]): tuple[x: int, y: int] =
    var x = start.x
    var y = start.y
    while x > 0 and y > 0:
        x.inc(-1)
        y.inc(-1)
        yield (x, y)

iterator topRight(start: tuple[x: int, y: int], maxX: int): tuple[x: int, y: int] =
    var x = start.x
    var y = start.y
    while x < maxX and y > 0:
        x.inc(1)
        y.inc(-1)
        yield (x, y)

iterator bottomLeft(start: tuple[x: int, y: int], maxY: int): tuple[x: int, y: int] =
    var x = start.x
    var y = start.y
    while x > 0 and y < maxY:
        x.inc(-1)
        y.inc(1)
        yield (x, y)

iterator bottomRight(start: tuple[x: int, y: int], maxX, maxY: int): tuple[x: int, y: int] =
    var x = start.x
    var y = start.y
    while x < maxX and y < maxY:
        x.inc(1)
        y.inc(1)
        yield (x, y)

proc numOccupiedSeats2(input: openArray[string]): int =
    var
        grid: seq[seq[char]]
        changeMade: int
        maxX = input[0].high
        maxY = input.high

    for line in input:
        grid.add(cast[seq[char]](line))

    var gridCopy = grid

    while true:
        changeMade = 0
        for x, y in grid(maxX, maxY):
            var surr: string
            var inView: int
            let current = grid[y][x]
            if current == flr:
                # no one sits on the floor
                continue

            for xi, yi in up((x, y)):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in down((x, y), maxY):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in left((x, y)):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in right((x, y), maxX):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in topLeft((x, y)):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in topRight((x, y), maxX):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in bottomLeft((x, y), maxY):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break
            for xi, yi in bottomRight((x, y), maxX, maxY):
                if grid[yi][xi] != flr:
                    if grid[yi][xi] == occ:
                        inView.inc
                    break

            if current == emp and inView == 0:
                gridCopy[y][x] = occ
                changeMade.inc
            elif current == occ and inView >= 5:
                gridCopy[y][x] = emp
                changeMade.inc
        if changeMade == 0:
            break
        else:
            grid = gridCopy
    
    for x, y in grid(maxX, maxY):
        if grid[y][x] == occ:
            result.inc

# tests
assert numOccupiedSeats(testInput[0]) == testInput[1]
assert numOccupiedSeats2(testInput[0]) == testInput[2]

let part1Answer = numOccupiedSeats(puzzleInput)
let part2Answer = numOccupiedSeats2(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 2289
assert part2Answer == 2059
