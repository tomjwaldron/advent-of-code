# debug build:
#   nim c -r day9.nim -opt:speed

# benchmark:
#   timeit day9.nim -d:release -opt:speed

import strutils, sequtils, algorithm, parseutils

const puzzleInput = readFile("day9.txt").splitLines().mapIt(it.parseInt)

const testInput = ("""35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576""".splitLines().mapIt(it.parseInt), 5, 127, 62)

func foundSum(preamble: openArray[int], target: int): bool =
    result = false
    for pre in preamble:
        if (target - pre) in preamble:
            result = true
            break

func firstInvalid(input: openArray[int], pre: int): int =
    var preamble = toSeq(input[0..<pre])
    for value in input[pre..^1]:
        if foundSum(preamble, value):
            preamble.rotateLeft(1)
            preamble[^1] = value
        else:
            return value

proc findWeakness(input: openArray[int], target: int): int =
    result = 0
    for i in 0..<(input.find(target) - 1):
        var counter, acc: int
        var list: seq[int]
        while (acc < target):
            var val = input[i + counter]
            acc += val
            list.add(val)
            counter.inc
        if acc == target:
            result = list.min + list.max
            break

assert firstInvalid(testInput[0], testInput[1]) == testInput[2]
assert findWeakness(testInput[0], testInput[2]) == testInput[3]

const part1Answer = firstInvalid(puzzleInput, 25)
const part2Answer = findWeakness(puzzleInput, part1Answer)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 1639024365
assert part2Answer == 219202240
