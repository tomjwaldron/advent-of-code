# debug build:
#   nim c -r day6.nim -opt:speed

# benchmark:
#   timeit day6.nim -d:release -opt:speed

import strutils, seqUtils, sets

const puzzleInput = readFile("day6.txt").replace("\c").split("\n\n")

const testInput = """abc

a
b
c

ab
ac

a
a
a
a

b""".split("\n\n")

for i, test in testInput:
    # echo i, " ", test
    let hash = toHashSet(test)
    let split = toHashSet(test.split)
    echo i, " ", hash, " ", hash.len
    echo i, " ", split, " ", split.len

# echo puzzleInput.foldl(a + toHashSet(b.split).len, 0)

# proc print[T](input: T): int =
#     echo input
#     return 1

# discard testInput.mapIt(print(it))

# echo testInput.countIt(toHashSet(it.filterIt(it != '\n')).len)

var p1Count, p2Count = 0

for input in puzzleInput:
    p1Count += input.filterIt(it != '\n').deduplicate().len

for input in puzzleInput:
    p2Count += input.split.foldl($intersection(toHashSet(a), toHashSet(b))).multiReplace(("{", ""), ("}", ""), ("'", ""), (", ", "")).len

proc intersect(input: string): int =
    input.split().foldl($intersection(toHashSet(a), toHashSet(b))).multiReplace(("{", ""), ("}", ""), ("'", ""), (", ", "")).len

const part1Answer = puzzleInput.foldl(a + toHashSet(b.filterIt(it != '\n')).len, 0)
let part2Answer = puzzleInput.foldl(a + b.intersect, 0)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 6903
assert part2Answer == 3493
