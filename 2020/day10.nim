# debug build:
#   nim c -r day10.nim -opt:speed

# benchmark:
#   timeit day10.nim -d:release -opt:speed

import strutils, sequtils, algorithm

const puzzleInput = readFile("day10.txt").splitLines().mapIt(it.parseInt)

const testInput = [("""16
10
15
5
1
11
7
19
6
12
4""".splitLines().mapIt(it.parseInt), 7 * 5, 8), ("""28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3""".splitLines().mapIt(it.parseInt), 22 * 10, 19208)]

proc getJoltageDist(input: openArray[int]): int =
    let adapters = input.sorted()
    var counter, diff1Count, diff3Count: int
    for a in adapters:
        let diff = a - counter
        counter = a
        if diff == 1: diff1Count.inc
        if diff == 2: echo "got here"
        elif diff == 3: diff3Count.inc
    diff1Count * (diff3Count + 1)

proc getNumArrangements(input: openArray[int]): int =
    let
        adapters = concat(@[0], input.sorted, @[input.max + 3])
        trib = [1, 1, 2, 4, 7, 13]
    var
        acc = 1
        current = 1
    for joltage in adapters:
        if joltage + 1 in adapters:
            current.inc
        else:
            acc *= trib[current - 1]
            current = 1
    result = acc

# tests
assert getJoltageDist(testInput[0][0]) == testInput[0][1]
assert getJoltageDist(testInput[1][0]) == testInput[1][1]
assert getNumArrangements(testInput[0][0]) == testInput[0][2]
assert getNumArrangements(testInput[1][0]) == testInput[1][2]

let part1Answer = getJoltageDist(puzzleInput)
let part2Answer = getNumArrangements(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 1700
assert part2Answer == 12401793332096
