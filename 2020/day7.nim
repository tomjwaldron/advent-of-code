# debug build:
#   nim c -r day7.nim -opt:speed

# benchmark:
#   timeit day7.nim -d:release -opt:speed

import pegs, strutils, parseutils, sequtils

const puzzleInput = readFile("day7.txt")

const testInput = [
    """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.""",
    """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""]

type bag = (string, seq[string])

proc parse(input: string): seq[bag] =
    for line in input.splitlines:
        var b: bag
        var myToken: string
        var size, loc: int
        size = parseUntil(line, myToken, " contain")
        loc.inc(size + " contain ".len)
        myToken.removeSuffix('s')
        myToken.removeSuffix(" bag")
        b[0] = myToken
        while true:
            size = parseUntil(line, myToken, {',', '.'}, loc)
            myToken.removeSuffix('s')
            myToken.removeSuffix(" bag")
            if size > 0: 
                b[1].add(myToken)
                loc.inc(size + 2)
            else: break
        result.add(b)

const interations = 100

proc numBagsContaining(input: seq[bag]): int =
    var canHold: array[interations, seq[string]]
    for item in input:
        for b in item[1]:
            if "shiny gold" in b:
                canHold[0].add(item[0])
                break
    for i in 1..<interations:
        for hold in canHold[i - 1]:
            for item in input:
                for b in item[1]:
                    if hold in b:
                        if not (item[0] in canHold[i]):
                            canHold[i].add(item[0])
                        break
    var results: seq[string]
    for hold in canHold:
        for b in hold:
            results.add(b)
    results = results.deduplicate()
    results.len

var counter = 0

proc numIndividualBags(input: seq[bag], bagColour: string, mul: int = 1): int =
    result = 0
    for i in input:
        if bagColour in i[0]:
            var inside = 0
            for bags in i[1]:
                let split = bags.split(" ", 1)
                counter.inc
                if "no" in split[0]:
                    counter.inc
                    return mul
                let num = numIndividualBags(input, split[1], split[0].parseInt)
                inside.inc(num)
                counter.inc
                result = mul + inside * mul

# tests
assert numBagsContaining(testInput[0].parse) == 4
assert numIndividualBags(testInput[0].parse, "shiny gold") - 1 == 32
assert numIndividualBags(testInput[1].parse, "shiny gold") - 1 == 126

let part1Answer = numBagsContaining(puzzleInput.parse)
let part2Answer = numIndividualBags(puzzleInput.parse, "shiny gold") - 1

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 192
assert part2Answer == 12128
