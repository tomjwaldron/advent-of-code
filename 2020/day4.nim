# debug build:
#   nim c -r day4.nim -opt:speed

# benchmark:
#   timeit day4.nim -d:release -opt:speed

import strutils, sequtils, pegs

const puzzleInput = readFile("day4.txt").replace("\c").split("\n\n")

const testInput = [
"""ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in""".split("\n\n"),

"""eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007""".split("\n\n"),

"""pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719""".split("\n\n")
]

func passportIsValidP1(passport: string): bool =
    const req = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    req.countIt(it in passport) == req.len

const valid = [
    proc (s: string): bool = s =~ peg"'byr:'{\d*}" and parseInt(matches[0]) in 1920..2002,
    proc (s: string): bool = s =~ peg"'iyr:'{\d*}" and parseInt(matches[0]) in 2010..2020,
    proc (s: string): bool = s =~ peg"'eyr:'{\d*}" and parseInt(matches[0]) in 2020..2030,
    proc (s: string): bool = s =~ peg"'hgt:'{\d*}{('in'/'cm')}" and (("cm" in matches[1] and parseInt(matches[0]) in 150..193) or ("in" in matches[1] and parseInt(matches[0]) in 59..76)),
    proc (s: string): bool = s =~ peg"'hcl:#'{[0-9a-f]+}" and matches[0].len == 6,
    proc (s: string): bool = s =~ peg"'ecl:'{[a-z]+}" and matches[0] in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"],
    proc (s: string): bool = s =~ peg"'pid:'{\d+}" and matches[0].len == 9,
    proc (s: string): bool = true
]

proc passportIsValidP2(passport: string): bool =
    const req2 = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"]
    result = true
    if passportIsValidP1(passport):
        for p in passport.split():
            for r in 0..<req2.len:
                if p[0..2] == req2[r]:
                    if not valid[r](p):
                        return false
    else:
        result = false

# tests
assert passportIsValidP1(testInput[0][0]) == true
assert passportIsValidP1(testInput[0][1]) == false
assert passportIsValidP1(testInput[0][2]) == true
assert passportIsValidP1(testInput[0][3]) == false
assert testInput[0].countIt(passportIsValidP1(it)) == 2
assert testInput[1].countIt(passportIsValidP2(it)) == 0
assert testInput[2].countIt(passportIsValidP2(it)) == 4

const part1Answer = puzzleInput.countIt(passportIsValidP1(it))
const part2Answer = puzzleInput.countIt(passportIsValidP2(it))

# for refactoring
assert part1Answer == 213
assert part2Answer == 147

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer
