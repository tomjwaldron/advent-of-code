# debug build:
#   nim c -r day3.nim -opt:speed

# benchmark:
#   timeit day3.nim -d:release -opt:speed

import strutils, sequtils

const puzzleInput = readFile("day3.txt").splitLines()

const testInput = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#""".splitLines()

func numTreesEncountered(input: openArray[string], incX: int, incY: int, width: int): int =
    var x = incX
    for y in countup(incY, len(input) - 1, incY):
        result += int(input[y][x] == '#')
        x = (x + incX) mod width

func multiSlopes(input: openArray[string], slopes: openArray[(int, int)]): int =
    slopes.foldl(a * numTreesEncountered(input, b[0], b[1], len(input[0])), 1)

const slopes = [(1, 1), (5, 1), (7, 1), (1, 2)]

# tests
assert numTreesEncountered(testInput, 3, 1, len(testInput[0])) == 7
assert multiSlopes(testInput, slopes) * 7 == 336

const part1Answer = numTreesEncountered(puzzleInput, 3, 1, len(puzzleInput[0]))
const part2Answer = multiSlopes(puzzleInput, slopes) * part1Answer

# 191
echo "Part 1: ", part1Answer

# 1478615040
echo "Part 2: ", part2Answer
