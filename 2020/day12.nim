# debug build:
#   nim c -r day12.nim -opt:speed

# benchmark:
#   timeit day12.nim -d:release -opt:speed

import strutils, math

const puzzleInput = readFile("day12.txt").splitlines

const testInput = ("""F10
N3
F7
R90
F11""".splitlines, 25, 286)

proc getManhattanDistance(input: openArray[string]): int =
    var
        x = 0
        y = 0
        dir = 0
    for inst in input:
        let move = inst[0]
        let value = parseInt(inst[1..^1])
        case move:
            of 'N':
                y.inc(value)
            of 'S':
                y.inc(-value)
            of 'E':
                x.inc(value)
            of 'W':
                x.inc(-value)
            of 'L':
                if dir - value < 0:
                    dir = 360 + (dir - value)
                else:
                    dir.inc(-value)
            of 'R':
                if dir + value >= 360:
                    dir = (dir + value) - 360
                else:
                    dir.inc(value)
            of 'F':
                if dir == 0:
                    x.inc(value)
                elif dir == 90:
                    y.inc(-value)
                elif dir == 180:
                    x.inc(-value)
                elif dir == 270:
                    y.inc(value)
            else:
                assert false, "unknown instruction"
    abs(x) + abs(y)

proc rotate(angle: float, pos: tuple[x, y: int]): tuple[x, y: int] =
    let
        s = sin(angle)
        c = cos(angle)
    result.x = int(round(float(pos.x) * c - float(pos.y) * s))
    result.y = int(round(float(pos.x) * s + float(pos.y) * c))

proc chaseWaypoint(input: openArray[string]): int =
    var
        wx = 10
        wy = 1
        x = 0
        y = 0
    for inst in input:
        let move = inst[0]
        let value = parseInt(inst[1..^1])
        case move:
            of 'N':
                wy.inc(value)
            of 'S':
                wy.inc(-value)
            of 'E':
                wx.inc(value)
            of 'W':
                wx.inc(-value)
            of 'L':
                (wx, wy) = rotate(degToRad(float(value)), (wx, wy))
            of 'R':
                (wx, wy) = rotate(degToRad(float(-value)), (wx, wy))
            of 'F':
                x.inc(value * wx)
                y.inc(value * wy)
            else:
                assert false, "unknown instruction"
    abs(x) + abs(y)

# tests
assert getManhattanDistance(testInput[0]) == testInput[1]
assert chaseWaypoint(testInput[0]) == testInput[2]

const part1Answer = getManhattanDistance(puzzleInput)
const part2Answer = chaseWaypoint(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 1177
assert part2Answer == 46530
