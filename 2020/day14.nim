# debug build:
#   nim c -r day14.nim -opt:speed

# benchmark:
#   timeit day14.nim -d:release -opt:speed

import strutils, parseutils, bitops, math, sequtils

const puzzleInput = readFile("day14.txt").splitLines

const testInput = [("""mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0""".splitLines, 165'u64), ("""mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1""".splitlines, 208'u64)]

proc updateMask(line: string, clrMsk, setMsk: var uint64) =
    let maskString = line.split[2]
    discard maskString.replace("X", "0").parseBin(setMsk)
    discard maskString.replace("1", "X").multiReplace(("0", "1"), ("X", "0")).parseBin(clrMsk)

proc parseInst(line: string): tuple[mem: uint64, val: uint64] =
    let
        split = line.split
        value = split[2].parseInt
        address = split[0][4..^2].parseInt
    (uint64(address), uint64(value))

proc part1(input: openArray[string]): uint64 =
    var
        memoryAddress: seq[uint64]
        memoryValue: seq[uint64]
        clrMsk, setMsk: uint64
        mem: uint64
        value: uint64

    for line in input:
        if "mask" in line:
            updateMask(line, clrMsk, setMsk)
        else:
            (mem, value) = parseInst(line)
            value.clearMask(clrMsk)
            value.setMask(setMsk)
            var writtenAddr = memoryAddress.find(mem)
            if writtenAddr != -1:
                memoryValue[writtenAddr] = value
            else:
                memoryAddress.add(mem)
                memoryValue.add(value)
    result = sum(memoryValue)

proc updateMasks(line: string): tuple[clrMsk, setMsk: seq[uint64]] =
    let
        maskString = line.split[2]
        lenMinus1 = maskString.len - 1
    var
        initMask: uint64

    discard maskString.replace("X", "0").parseBin(initMask)

    var x: seq[int]
    for i, c in maskString.pairs:
        if c == 'X':
            x.add(lenMinus1 - i)
    
    let numCombinations: uint64 = 2'u64 ^ x.len
    var combinations = toSeq(0'u64..<numCombinations)

    for c in combinations:
        var setVal, clrVal: uint64

        for i, pos in x.pairs:
            if c.testBit(i):
                setVal.setBit(pos)
            else:
                clrVal.setBit(pos)

        result.setMsk.add(setVal)
        result.clrMsk.add(clrVal)

    for m in result.setMsk.mitems:
        m.setMask(initMask)

proc part2(input: openArray[string]): uint64 =
    var
        memoryAddress: seq[uint64]
        memoryValue: seq[uint64]
        clrMasks: seq[uint64]
        setMasks: seq[uint64]
        mem: uint64
        value: uint64

    for line in input:
        if "mask" in line:
            (clrMasks, setMasks) = updateMasks(line)
        else:
            (mem, value) = parseInst(line)
            var maskMem = mem
            for i in 0 ..< clrMasks.len:
                maskMem.clearMask(clrMasks[i])
                maskMem.setMask(setMasks[i])

                var writtenAddr = memoryAddress.find(maskMem)
                if writtenAddr != -1:
                    memoryValue[writtenAddr] = value
                else:
                    memoryAddress.add(maskMem)
                    memoryValue.add(value)

    result = sum(memoryValue)

# tests
assert part1(testInput[0][0]) == testInput[0][1]
assert part2(testInput[1][0]) == testInput[1][1]

let part1Answer = part1(puzzleInput)
let part2Answer = part2(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 6631883285184'u64
assert part2Answer == 3161838538691'u64
