# debug build:
#   nim c -r day13.nim -opt:speed

# benchmark:
#   timeit day13.nim -d:release -opt:speed

import strutils, sequtils, math

const puzzleInput = readFile("day13.txt").splitLines

const testInput = ("""939
7,13,x,x,59,x,31,19""".splitlines, 295, 1068781)

proc part1(input: openArray[string]): int =
    let
        time = parseInt(input[0])
        busIDs = input[1].split(",").filterIt(not ('x' in it)).mapIt(it.parseInt)
    var
        waitTime = -1
        busToTake = 0
    while busToTake == 0:
        waitTime.inc
        for id in busIDs:
            if (time + waitTime) mod id == 0:
                busToTake = id
    waitTime * busToTake

proc part2(input: openArray[string]): int =
    let
        split = input[1].split(",")
        busID = split.filterIt(not ('x' in it)).mapIt(it.parseInt)
        busIdx = busID.mapIt(split.find($it))
    var 
        acc = busID[0]
        x = acc

    for bus in 0 ..< busID.len - 1:
        let slice = busID[0..bus]
        acc = lcm(slice)

        # find value that mod == 0
        while (x + busIdx[bus + 1]) mod busID[bus + 1] != 0:
            x += acc

    result = x

# tests
assert part1(testInput[0]) == testInput[1]
assert part2(testInput[0]) == testInput[2]

let part1Answer = part1(puzzleInput)
let part2Answer = part2(puzzleInput)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 2298
assert part2Answer == 783685719679632
