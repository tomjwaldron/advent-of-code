# debug build:
#   nim c -r day5.nim -opt:speed

# benchmark:
#   timeit day5.nim -d:release -opt:speed

import strutils, sequtils, parseutils

const puzzleInput = readFile("day5.txt").splitLines()

const testInput = [
    ("FBFBBFFRLR", 357),
    ("BFFFBBFRRR", 567),
    ("FFFBBBFRRR", 119),
    ("BBFFBBFRLL", 820)
]

func getID(pass: string): int =
    discard parseBin(pass.multiReplace(("F", "0"), ("B", "1"), ("L", "0"), ("R", "1")), result)

# tests
for test in testInput:
    assert getID(test[0]) == test[1]

const seatIDs = puzzleInput.mapIt(getID(it))

const part1Answer = max(seatIDs)
const part2Answer = seatIDs.foldl(a xor b)

echo "Part 1: ", part1Answer
echo "Part 2: ", part2Answer

assert part1Answer == 933
assert part2Answer == 711
